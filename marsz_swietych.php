<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>MARSZ ŚWIĘTYCH</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="marsz-swietych-section">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="marsz-swietych-content">
                            <h2>MARSZ ŚWIĘTYCH 2018</h2>
                            <p>
                                31 października o godzinie 18.00 Mszą Świętą z Uroczystości Wszystkich Świętych w naszym
                                kościele rozpoczął się tegoroczny Marsz. Każdy z nas jest zaproszony do świętości, każdy
                                z nas ma być świętym – mówił w kazaniu ksiądz Paweł i dlatego wśród wizerunków świętych
                                można było znaleźć lusterko. Po Mszy Świętej w procesji za krzyżem przeszliśmy przez
                                opolski Rynek, by na schodach kościoła zakończyć nasz Marsz, zabierając z sobą losowo
                                wybrany wizerunek świętego na kolejny rok...
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 img"
                         style="background-image: url('assets/img/duszpasterstwo/marsz_swietych/ms_2018.png');">
                    </div>
                </div>
            </div>
        </section>
    </article>
<?php
$pageTitle = 'Marsz Świętych - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>