<?php
if(@$_SESSION['admin']==1 && $zalogowany=1){
$zgloszenia = 'select kontakt.* from kontakt order by data_kontaktu desc;';
$z = $db->query($zgloszenia);
?>

<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>WIADOMOŚCI UŻYTKOWNIKÓW</h1>
                <div class="divider-h"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="zgloszenia-section">
        <div class="container">
            <?php
            foreach ($z as $zg) {
                echo '
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-4">								
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><b>Nadawca:</b> ' . $zg['imie_nazw_kontaktu'] . '</li>
                                        <li class="list-group-item"><b>Adres e-mail:</b> ' . $zg['email_kontaktu'] . '</li>
                                        <li class="list-group-item"><b>Data przesłania:</b> ' . $zg['data_kontaktu'] . '</li>
                                        <li class="list-group-item"><button class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#exampleModal">Usuń</button></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-md-8">                      
                                    <p class="card-text" style="padding: .75rem 1.25rem;"><b>Treść wiadomości</b><br>' . $zg['tresc_kontaktu'] . '</p>
                                </div>
                            </div>   
                        </div>             
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Uwaga!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Czy na pewno chcesz usunąć to zgłoszenie?
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-danger" href="./adminpanel.zgloszenia/' . $zg['id_kontaktu'] . '/usun/">Tak</a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>
                                </div>
                            </div>
                        </div>
                    </div>';
            }
				if(isset($params[1]) && $params[1] == 'usun')
	{
		//print_r($_POST['wybor_powodu']);
		//print_r($_SESSION['id_uzytkownika']);
		$query_usun = 'DELETE FROM kontakt where id_kontaktu='.$params[0];
		$db->query($query_usun);
	header('Location: /kosciolnagorce/adminpanel/');
	}

            ?>
        </div>
    </section>
</article>
<?php
}
else
{
	include 'blad.php';
}
?>




