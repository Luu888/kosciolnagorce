<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>BIERZMOWANIE</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                I wszyscy zostali napełnieni Duchem Świętym, i zaczęli mówić
                                obcymi językami tak im Duch pozwalał mówić
                            </p>
                            Dz 2,4
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/bierzmowanie.jpg" style="margin-bottom: 30px;" alt="chrzest">
                        <div class="sakramenty-content">
                            <p>
                                Sakramentem, poprzez który człowiek otrzymuje Ducha Świętego jest bierzmowanie.
                                Przygotowanie do przyjęcia bierzmowania rozpoczyna się w szóstej klasie szkoły
                                podstawowej i
                                trwa 3 lata. Harmonogram całej formacji jest przedstawiany na pierwszym spotkaniu.
                            </p>
                            <p>
                                Jeżeli chcesz zapisać się do przyjęcia sakramentu bierzmowania
                                <a href="formularze">tutaj</a> znajdziesz przeznaczony do tego formularz.
                            </p>
                        </div>
                    </div>
                </div>
        </section>

    </article>
<?php
$pageTitle = 'Bierzmowanie - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>