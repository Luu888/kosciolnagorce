<?php
if (@$_SESSION['admin'] == 1 && $zalogowany = 1) {
    $zgloszenia = 'select malzenstwo.* from malzenstwo order by id desc;';
    $z = $db->query($zgloszenia);
    ?>

    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeIn">
                    <h1>ZGŁOSZENIA MAŁŻEŃSTWA</h1>
                    <div class="divider-h"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="zgloszenia-section">
            <div class="container">
                <div class="card-columns">
                    <?php
                    foreach ($z as $zg) {
                        echo '
                    <div class="card">
                        <div class="card-body">									
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><b>Imię:</b> ' . $zg['imie_slub'] . '</li>
                                <li class="list-group-item"><b>Nazwisko</b> ' . $zg['nazwisko_slub'] . '</li>
                                <li class="list-group-item"><b>Adres e-mail</b> ' . $zg['email_slub'] . '</li>
                                <li class="list-group-item"><b>Telefon</b> ' . $zg['telefon_slub'] . '</li>	
                                <li class="list-group-item"><b>Godzina ślubu: </b>' . $zg['godzina_slub'] . '</li>	
                                <li class="list-group-item"><b>Data ślubu: </b>' . $zg['data_slub'] . '</li>								
                            </ul>
                        </div>  
                        <div class="col-12">
                            <button class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#exampleModal" style="margin-bottom: 10px;">Usuń</button>                            
                        </div>
                    </div>

                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Uwaga!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        Czy na pewno chcesz usunąć to zgłoszenie?
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-danger" href="./adminpanel.malzenstwo/' . $zg['id'] . '/usun/" >Tak</a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>
                                </div>
                            </div>
                        </div>
                    </div>';
                    }
                    if (isset($params[1]) && $params[1] == 'usun') {

                        $query_usun = 'DELETE FROM malzenstwo where id=' . $params[0];
                        $db->query($query_usun);
                        header('Location: /kosciolnagorce/adminpanel.malzenstwo/');
                    }



                    ?>
                </div>
            </div>
        </section>
    </article>


    <?php
} else {
    include 'blad.php';
}
?>




