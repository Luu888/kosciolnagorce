<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>OGŁOSZENIA PARAFIALNE</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="ogloszenia-section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <img src="assets/img/ogloszenia.jpg" style="margin-bottom: 30px;">
                        <h1>IV Niedziela Wielkanocna 03.05.2020</h1>
                        <p>
                            W mocy pozostaje dyspensa od udziału we Mszy Świętej. Adekwatnie do powierzchni kościoła
                            wynoszącej 640 m2 w naszej świątyni mogą przebywać 42 osoby. Aby umożliwić jak
                            największej
                            ilości chętnych udział we Mszy w dzisiejszą niedzielę zostaną odprawione dodatkowe msze
                            o
                            godz. 15.30 i jeżeli będzie taka potrzeba również o 19.00. WSZYSCY OBECNI W KOŚCIELE SĄ
                            ZOBOWIĄZANI DO ZAKRYWANIA TWARZY.
                        </p>
                        <p class="gray">
                            Powracamy do tradycyjnej formy korzystania z Sakramentu Pokuty. Spowiedź odbywa się w
                            konfesjonale z obowiązkowym zakrywaniem nosa i ust. Zachęcamy zatem do skorzystania ze
                            Spowiedzi Wielkanocnej, która w naszej świątyni jest możliwa pół godziny przed każdą Mszą
                            Świętą za wyjątkiem środy gdy jest Nowenna do MBNP.
                        </p>
                        <p>
                            W tym tygodniu przeżywamy pierwszy czwartek miesiąca.
                        </p>
                        <p class="gray">
                            Nabożeństwa majowe odprawiamy codziennie o 17.30 a w środy o 17.15 razem z Nowenną do MBNP.
                        </p>
                        <p>
                            Gorąco dziękujemy wszystkim wspierającym naszą parafię. Kolekta z minionej niedzieli
                            wyniosła 1904,95 pln oraz 0,01 euro.
                        </p>
                        <p class="gray">
                            Po każdej Mszy Św. w dni powszednie modlimy się za wstawiennictwem św. Judy Tadeusza o
                            zatrzymanie epidemii oraz w intencji pracowników Służby Zdrowia.
                        </p>
                        <p>
                            W odpowiedzi na apel Episkopatu Polski do gorliwej modlitwy o wygaśnięcie epidemii
                            zachęcamy do indywidualnej adoracji Najświętszego Sakramentu, która w naszym kościele
                            możliwa jest w dni powszednie w godz. 7.00 - 17.30. Aby adoracja trwała nieustannie
                            zachęcamy do zapisywania się przez formularz dostępny na naszej stronie parafialnej.
                        </p>
                        <p class="gray">
                            Zachęcamy do lektury Gościa Niedzielnego i naszej gazetki parafialnej, która jest dostępna w
                            zakrystii.
                        </p>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'OgŁoszenia - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>