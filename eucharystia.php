<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>EUCHARYSTIA</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                Ja jestem chlebem żywym, który zstąpił z nieba. Jeśli kto spożywa ten chleb, będzie
                                żył…
                            </p>
                            J 6,51
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/eucharystia.jpg" style="margin-bottom: 30px;">
                        <div class="sakramenty-content">
                            <p>
                                Jeżeli chcesz doświadczyć prawdziwego życia zapraszamy Cię na Eucharystię każdego dnia w
                                następujących godzinach:
                            </p>
                            <p>
                                <b>Poniedziałek - Piątek</b><br>
                                6:30 | 12:00 | 18:00 <br>
                                <b>Sobota</b><br>
                                6.30 | 18:00<br>
                                <b>Niedziela</b><br>
                                7.30 | 9.00 | 10:30 | 12:00 | 18:00<br>
                            </p>
                            <p>
                                Jeżeli chcesz aby Twoje dziecko przygotowywało się do I Komunii Świętej w naszej parafii
                                lub
                                chcesz zasięgnąć bieżących informacji o spotkaniach kliknij
                                <a href="formularze">TUTAJ</a>.
                            </p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="sakramenty-cytat" style="margin-top: 30px;">
                            <p>
                                Czas ofiarowany Chrystusowi nigdy nie jest czasem straconym, ale raczej czasem, który
                                zyskujemy, aby nadać głęboko ludzki charakter naszym relacjom z innymi i naszemu
                                życiu
                            </p>
                            Św. Jan Paweł II
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Eucharystia - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>