<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>NAMASZCZENIE CHORYCH</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                Choruje ktoś wśród was? Niech sprowadzi kapłanów Kościoła, by się modlili nad nim i
                                namaścili go olejem w imię Pana A modlitwa pełna wiary będzie dla chorego ratunkiem i
                                Pan go podźwignie, a jeśliby popełnił grzechy, będą mu odpuszczone
                            </p>
                            Jk 5,14
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/namaszczenie.jpg" style="margin-bottom: 30px;">
                        <div class="sakramenty-content">
                            <p>
                                Sakrament namaszczenia chorych jest udzielany w naszej parafii zazwyczaj podczas
                                odwiedzin
                                chorych oraz w przypadku osób umierających po zgłoszeniu telefonicznym.
                            </p>
                            <p>
                                <b>Najbliższe odwiedziny chorych:</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Namaszczenie chorych - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>