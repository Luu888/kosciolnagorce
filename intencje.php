<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>INTENCJE MSZALNE</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="intencje-section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <img src="assets/img/intencje.jpg" style="margin-bottom: 30px;">
                        <h2>Niedziela 22.03.2020</h2>
                        <h2>IV NIEDZIELA Wielkiego Postu</h2>
                        <p>
                            <b>7.00</b> Godzinki ku czci Niepokalanego Poczęcia NMP <br>
                            <b>7.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>9.00</b> Za + siostrę i ciocię Jadwigę Dobosz w 6 rocznicę śmierci <br>
                            <b>10.30</b> 1) W intencji Rodzin Magdów, Urbańskich i Piglowskich z podziękowaniem za otrzymane
                            łaski, z prośbą o dalszą opiekę i zdrowie <br>
                            2) Za + Leonarda w 38 rocznicę śmierci i Wandę Kupińskich, Zbigniewa Kwiatkowskiego i Zofię
                            Masztalerz, i wszystkich ++ z rodziny z obu stron o życie wieczne dla nich <br>
                            <b>12.00</b> W intencji dziękczynnej w rocznice urodzin Grzegorza i jego córki Leny z prośbą o
                            dary
                            Ducha Świętego i dalsze Boże błogosławieństwo <br>
                            <b>Chrzest:</b> Gloria Stanisława Eichhorn <br>
                            <b>17.15</b> Gorzkie Żale <br>
                            <b>18.00</b> Za + Sylwię Gackowską o życie wieczne dla niej
                        </p>

                        <h2>Poniedziałek 23.03.2020</h2>
                        <p>
                            <b>6.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>12.00</b> Msza studencka <br>
                            <b>17.30</b> Różaniec <br>
                            <b>18.00</b> Za + Mieczysława Kurendę w rocznicę śmierci i za ++ rodziców z obu stron o życie
                            wieczne dla nich
                        </p>


                        <h2>Wtorek 24.03.2020</h2>
                        <p>
                            <b>6.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>12.00</b> Msza studencka <br>
                            <b>17.30</b> Różaniec <br>
                            <b>18.00</b> 1) O Boże błogosławieństwo i dar potomstwa dla Adriana i Ani <br>
                            2) za śp. ks. Edwarda Kucharza
                        </p>


                        <h2>Środa 25.03.2020</h2>
                        <p>
                            <b>6.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>12.00</b> Msza studencka <br>
                            <b>17.15</b> Nowenna za wstawiennictwem MBNP <br>
                            <b>18.00</b> Msza w intencjach polecanych w Nowennie
                        </p>


                        <h2>Czwartek 26.03.2020</h2>
                        <p>
                            <b>6.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>12.00</b> Msza studencka <br>
                            <b>17.30</b> Różaniec <br>
                            <b>18.00</b> 1) Za++ Marka Chmurowskiego, ojca i dziadków i z ++ rodziny Chmurowskich <br>
                            2) Za + Bożenę Turkowską w 10 rocznicę śmierci (od koleżanek i kolegów)
                        </p>


                        <h2>Piątek 27.03.2020</h2>
                        <p>
                            <b>6.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>12.00</b> Msza studencka <br>
                            <b>17.15</b> Droga Krzyżowa <br>
                            <b>18.00</b> O szczęście wieczne dla + ojca Piotra w 50 rocznicę śmierci jego braci Jacka
                            i Bronisława stryjenkę Józefę
                        </p>

                        <h2>Sobota 28.03.2020</h2>
                        <p>
                            <b>6.30</b> Msza gregoriańska za + mamę Katarzynę Denderes <br>
                            <b>17.30</b> Nabożeństwo do Niepokalanego Serca NMP <br>
                            <b>18.00</b> Msza w intencjach zbiorowych <br>
                            <b>Chrzest:</b> Dominika Michalina Gumbrewicz
                        </p>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Intencje - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>