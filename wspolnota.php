<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>WSPÓLNOTA</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="wspolnota-section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <img src="assets/img/wspolnota.jpg" style="margin-bottom: 30px;">
                        <div class="wspolnota-content">
                            <p>
                                Parafia ma urzeczywistniać wspólnotowy wymiar Kościoła. poprzez parafię chrześcijanie
                                mają mieć możliwość doświadczenia, że Kościół do którego należą jest jednym ciałem i
                                prawdziwą wspólnotą. Jednak jak osiągnąć ten cel, kiedy poszczególni członkowie tej
                                wspólnoty nie znają się nawzajem? Na to pytanie nie mamy jeszcze odpowiedzi. Choć znamy
                                cel, to jednak nie znamy drogi, która do celu prowadzi. Ufamy jednak, że Bóg będzie nam
                                ukazywał ścieżki wiodące do coraz doskonalszego urzeczywistniania Jego zamiarów.
                            </p>
                            <p>
                                Jedną choć z pewnością nie najdoskonalszą formą tworzenia wspólnoty są działające grupy
                                parafialne - jeżeli chcesz przyłącz się do jednej z nich. Więcej inforamcji na ich temat
                                znajdziesz <a href="grupy_parafialne">tutaj</a>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Wspolnota - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>