<?php

?>
<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>NASZA PARAFIA</h1>
                <div class="divider-h wow fadeInUp"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="parafia-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="assets/img/kontakt/top.jpg" style="margin-bottom: 30px;">
                    <div class="parafia-content">
                        <h2>Do naszej parafii przynależą następujące ulice:</h2>
                        <p>
                            1 Maja (numery nieparzyste do 29), Armii Krajowej (numery 1-3), Damrota, pl. Daszyńskiego,
                            Dzierżonia, Graniczna, Św. Jacka, Kamienna, Kołłątaja, Konduktorska, pl. Kopernika,
                            Kościuszki
                            (numery nieparzyste 1-21), Krawiecka, Krupnicza, Luboszycka (numery nieparzyste 1-9 i
                            parzyste
                            2-8),
                            Malczewskiego, Mały Rynek, Nysy Łużyckiej nr 32, Osmańczyka, Ozimska (numery nieparzyste
                            1-23 i
                            parzyste 2-46), Podgórna, Reymonta, Rzemieślnicza, Sempołowskiej, Staromiejska, Struga,
                            Studzienna,
                            Targowa, Waryńskiego, Św. Wojciecha, Żeromskiego.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>

<?php
$pageTitle = 'Parafia - Parafia "na Górce"';
?>
</html>
<?php
include 'title.php';
?>
