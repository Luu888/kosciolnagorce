<?php
session_start();
ob_start();
error_reporting(E_ALL);

if (!isset($_SESSION['zalogowany'])) $_SESSION['zalogowany'] = 0;

$request = str_replace('/kosciolnagorce/', '', $_SERVER['REQUEST_URI']);
$params = explode('/', $request);

if ($params[0] == 'wyloguj' && $_SESSION['zalogowany'] == 1) {
    session_destroy();
    header("Location: /kosciolnagorce/");
}

$db = new mysqli('localhost', 'root', '', 'kosciolnagorce');
$db->set_charset("utf8");

$site = 'glowna';
if (!empty($params[0]))
    $site = array_shift($params);
$site_filename = $site . '.php';
if (!is_file($site_filename))
    $site_filename = 'blad.php';

$zalogowany = $_SESSION['zalogowany'] == 1;
?>
<!DOCTYPE html>
<base href="http://localhost/kosciolnagorce/">
<html lang="pl">
<head>
    <title>Strona główna - Parafia "na Górce"</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Jost:wght@300;500&display=swap">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Marcellus+SC&subset=latin,latin-ext">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css">

    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/media-queries.css">
    <link rel="stylesheet" href="assets/css/hamburger.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/2142690-christianity/2142690-christianity/font/flaticon.css">
    <link rel="stylesheet" href="assets/css/ekko-lightbox.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">

    <link rel="shortcut icon" href="assets/ico/favicon.png">
</head>
<body>

<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-6 col-sm-8">

            </div>
            <div class="col-6 col-sm-4">
                <div class="row">
                    <div id="ytd" class="col-12 col-xl-6 top-element">
                        <a id="yta" href="https://www.youtube.com/channel/UC9CH_7GOsX4WXNKA7AZOUTg" target="_blank" style="color: #FFFFFF;">
                            <i id ="yti" class="fab fa-youtube"></i>
                            NASZ KANAŁ YT
                        </a>
                    </div>
                    <div class="col-12 col-xl-6 top-element">
                        <i class="fas fa-phone-alt"></i>
                        +48 736 157 099
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<nav id="navbar" class="navbar navbar-dark sticky-top navbar-expand-lg navbar-no-bg">
    <div class="container nav">
        <button id="hamburger" class="hamburger hamburger--spring" type="button" data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link scroll-link" href="./">START</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./ogloszenia">OGŁOSZENIA</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./intencje">INTENCJE</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownDuszpasterstwo" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">DUSZPASTERSTWO</a>
                    <ul id="dropdownDuszpasterstwo" class="dropdown-menu" aria-labelledby="dropdownDuszpasterstwo">
                        <li><a class="dropdown-item" href="./grupy_parafialne">GRUPY PARAFIALNE</a></li>
                        <li class="dropdown-submenu">
                            <a id="dropdownSakramenty" href="#" role="button" data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false" class="dropdown-item dropdown-toggle">SAKRAMENTY</a>
                            <ul aria-labelledby="dropdownSakramenty" class="dropdown-menu">
                                <div class="dropdown-divider"></div>
                                <li><a class="dropdown-item" href="./chrzest">CHRZEST</a></li>
                                <li><a class="dropdown-item" href="./bierzmowanie">BIERZMOWANIE</a></li>
                                <li><a class="dropdown-item" href="./eucharystia">EUCHARYSTIA</a></li>
                                <li><a class="dropdown-item" href="./pokuta">POKUTA</a></li>
                                <li><a class="dropdown-item" href="./namaszczenie">NAMASZCZENIE CHORYCH</a></li>
                                <li><a class="dropdown-item" href="./kaplanstwo">KAPŁAŃSTWO</a></li>
                                <li><a class="dropdown-item" href="./malzenstwo">MAŁŻEŃSTWO</a></li>
                                <div class="dropdown-divider"></div>
                            </ul>
                        </li>
                        <li><a class="dropdown-item" href="">GAZETKA</a></li>
                        <li><a class="dropdown-item" href="./parafia">PARAFIA</a></li>
                        <li><a class="dropdown-item" href="galeria">GALERIA</a></li>
                        <li><a class="dropdown-item" href="./historia">HISTORIA</a></li>
                        <li><a class="dropdown-item" href="./pielgrzymki">PIELGRZYMKI</a></li>
                        <li><a class="dropdown-item" href="./marsz_swietych">MARSZ ŚWIĘTYCH</a></li>
                        <li><a class="dropdown-item" href="">PATRONI</a></li>
                        <li><a class="dropdown-item" href="">RELIKWIE</a></li>
                        <li><a class="dropdown-item" href="">FINANSE</a></li>
                        <li><a class="dropdown-item" href="">DUSZPASTERZE</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./ewangelizacja">EWANGELIZACJA</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./wspolnota">WSPÓLNOTA</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="./kontakt">KONTAKT</a>
                </li>
                <?php
                    if (@$zalogowany == 1 && $_SESSION['admin'] == 1)
                        {
                            echo '<li class="nav-item">';
                            echo '<a class="nav-link" href="./adminpanel">PANEL</a>';
                            echo '</li>';
                            echo '<li class="nav-item">';
                            echo '<a class="nav-link" href="./wyloguj">WYLOGUJ</a>';
                            echo '</li>';
                        }
                    else
                        {
                            echo '<li class="nav-item">';
                            echo '<a class="nav-link" href="./logowanie">ZALOGUJ</a>';
                            echo '</li>';
                        }
               ?>
            </ul>
        </div>
    </div>
</nav>

<?php
if ($site != 'glowna') {
    include $site_filename;

} else {
    include $site_filename;
}
?>
<footer>
    <div class="container">

        <div class="row">
            <div class="col footer-center">
                <p>&copy; 2020 Parafia Matki Boskiej Bolesnej i św. Wojciecha w Opolu</p>
                <p style="opacity: 0.3; font-size: 12px">Icon made by Darius Dan from www.flaticon.com</p>
            </div>
        </div>

    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="assets/js/hamburger.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>

<script type="text/javascript">
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>

<script src="assets/js/wow.min.js"></script>
<script>
    new WOW().init();
</script>

<script src="assets/js/swiper.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 5,
        spaceBetween: 30,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
        loop: true,
        // loopFillGroupWithBlank: true,
        breakpoints: {
            992: {
                slidesPerView: 4
            },
            768: {
                slidesPerView: 3
            },
            480: {
                slidesPerView: 2
            },
            320: {
                slidesPerView: 1
            }
        }
    });
</script>

<script>
    $(function () {
        $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function (event) {
            event.preventDefault();
            event.stopPropagation();

            $(this).siblings().toggleClass("show");


            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

        });
    });
</script>

<script>
    $(function(){
        $("#ytd").on("mouseover", function(){
            $("#yti").css("color", "#932929");
        });
        $("#ytd").on("mouseout", function(){
            $("#yti").css("color", "#FFFFFF");
        });
    });
</script>

</body>