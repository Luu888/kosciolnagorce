<?php

?>
<header>
    <div class="container">
        <div class="row">
            <div class="col wow fadeInDown">
                <h1>PIELGRZYMKI</h1>
                <div class="divider-h wow fadeInDown"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="pielgrzymki-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="assets/img/duszpasterstwo/grupy/top.jpg" style="margin-bottom: 40px;">
                    <nav>
                        <div class="nav justify-content-center" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="ziemia-swieta-tab"
                               data-toggle="tab"
                               href="#nav-ziemia-swieta"
                               role="tab" aria-controls="nav-ziemia-swieta" aria-selected="true">ZIEMIA ŚWIĘTA 2020</a>
                            <a class="nav-item nav-link" id="pielgrzymka2-tab" data-toggle="tab"
                               href="#nav-pielgrzymka2"
                               role="tab" aria-controls="nav-pielgrzymka2" aria-selected="false">PIELGRZYMKA 2</a>
                            <a class="nav-item nav-link" id="pielgrzymka3-tab" data-toggle="tab"
                               href="#nav-pielgrzymka3"
                               role="tab" aria-controls="nav-pielgrzymka3" aria-selected="false">PIELGRZYMKA 3</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-ziemia-swieta" role="tabpanel"
                             aria-labelledby="nav-ziemia-swieta-tab">
                            <div class="pielgrzymki-content">
                                <h1>ZIEMIA ŚWIĘTA 2020</h1>
                                W dniach 15-22 lutego 2020 r. nasza parafia pielgrzymowała do ziemskiej Ojczyzny
                                Chrystusa. W ramach pielgrzymki odwiedziliśmy m.in.: Nazaret, Betlejem, Górę
                                Błogosławieństw, Górę Tabor, Kanę Galilejską, Jezioro Galilejskie oraz Jerozolimę.
                                Wizyta w każdym z nich była okazją do bezpośredniego spotkania z miejscami uświęconymi
                                obecnością Zbawiciela. Zapraszamy do obejrzenia naszej fotorelacji pod tym <a
                                        href="galeria.wyswietl/Zdjecia%202">linkiem</a>.
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-pielgrzymka2" role="tabpanel"
                             aria-labelledby="nav-pielgrzymka2-tab">
                            <div class="pielgrzymki-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-pielgrzymka3" role="tabpanel"
                             aria-labelledby="nav-pielgrzymka3-tab">
                            <div class="pielgrzymki-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</article>

<?php
$pageTitle = 'Pielgrzymki - Parafia "na Górce"';
?>
</html>
<?php
include 'title.php';
?>
