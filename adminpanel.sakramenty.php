<?php
if(@$_SESSION['admin']==1 && $zalogowany=1){
$zgloszenia = 'select sakramenty.* from sakramenty order by id desc;';
$z = $db->query($zgloszenia);
?>

<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>ZGŁOSZENIA NA SAKRAMENTY</h1>
                <div class="divider-h"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="zgloszenia-section">
        <div class="container">
            <?php
            foreach ($z as $zg) {
                echo '
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-4">								
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><b>Imię:</b> ' . $zg['imie'] . '</li>
                                        <li class="list-group-item"><b>Adres e-mail:</b> ' . $zg['nazwisko'] . '</li>
                                        <li class="list-group-item"><b>Adres:</b> ' .$zg['ulica']. ' '.$zg['nr_domu']. ' ' .$zg['numer_mieszkania'].' '.$zg['miejscowosc'].' ' .$zg['kod_pocztowy'].'</li>
                                        <li class="list-group-item"><b>SAKRAMENT:</b> ' . $zg['nazwa_sakramentu'] . '</li>	
                                        <li class="list-group-item"><b>Data przesłania: </b>' . $zg['data_przeslania'] . '</li>									
                                    </ul>
                                </div>
                                <div class="col-12 col-md-4"> 
										<ul class="list-group list-group-flush">								
											<li class="list-group-item"><b>Szkoła:</b> ' . $zg['szkola'] . '</li>
											<li class="list-group-item"><b>Klasa:</b> ' . $zg['klasa'] . '</li>
											<li class="list-group-item"><b>Nazwisko katechety:</b> ' . $zg['nazwisko_katechety'] . '</li>
											<li class="list-group-item"><b>Imię katechety:</b> ' . $zg['imie_katechety'] . '</li>
										</ul>
                                </div>
								<div class="col-12 col-md-4">                      
                                    <ul class="list-group list-group-flush">
										<li class="list-group-item"><b>Nazwisko zgłaszającego:</b> ' . $zg['nazwisko_zglaszajacego'] . '</li>
										<li class="list-group-item"><b>Imię zgłaszającego:</b> ' . $zg['imie_zglaszajacego'] . '</li>
										<li class="list-group-item"><b>E-mail zgłaszającego:</b> ' . $zg['email_zglaszajacego'] . '</li>
										<li class="list-group-item"><b>Numer telefonu zgłaszającego:</b> ' . $zg['numer_telefonu'] . '</li>
										<li class="list-group-item"><b>Deklaracja o wychowaniu: </b>' . $zg['deklaracja'] . '</li>
									</ul>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#exampleModal">Usuń</button>   
                                </div>
                            </div>   
                        </div>             
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Uwaga!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                     Czy na pewno chcesz usunąć to zgłoszenie?
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-danger" href="./adminpanel.sakramenty/' . $zg['id'] . '/usun/" >Tak</a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>
                                </div>
                            </div>
                        </div>
                    </div>';
            }
				if(isset($params[1]) && $params[1] == 'usun')
	{
		
		$query_usun = 'DELETE FROM sakramenty where id='.$params[0];
		$db->query($query_usun);
	header('Location: /kosciolnagorce/adminpanel.sakramenty/');
	}

            ?>
        </div>
    </section>
</article>
<?php
}
else
{
	include 'blad.php';
}
?>




