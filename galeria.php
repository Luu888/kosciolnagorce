<?php
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col wow fadeIn">
                <h1>GALERIA</h1>
                <div class="divider-h"><span></span></div>
            </div>
        </div>
    </div>
</header>
<article>
    <div class="galeria-section">
        <div class="container">
            <div class="row">
                <?php
                $path = 'gallery/';
                $results = scandir($path);

                foreach ($results as $result) {
                    if ($result === '.' or $result === '..') continue;

                    if (is_dir($path . '/' . $result)) {
                        $firstFile = scandir($path . '/' . $result)[2];
                        echo "<div class='col-6 col-md-3'>";
                        echo "<a href='galeria.wyswietl/" . $result . "/'><img src='gallery/" . $result . "/" . $firstFile . "' width='200'></a>";
                        echo "<br>";
                        echo "<a href='galeria.wyswietl/" . $result . "/'>" . $result . "</a><br>";
                        echo "</div>";
                    }
                }
                ?>
            </div>
        </div>
    </div>
</article>