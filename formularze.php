<?php

$wyslano = false;

//if (isset($_POST['zgodaDane'])&& isset($_POST['deklaracja'])) {
if (isset($_POST['kandydatImie']) && isset($_POST['kandydatNazwisko'])) {
    $data_przeslania = date('Y-m-d');
    $kandytat_imie = $_POST['kandydatImie'];
    $kandytat_nazwisko = $_POST['kandydatNazwisko'];
    $kandytat_ulica = $_POST['kandydatUlica'];
    $kandytat_dom = $_POST['kandydatDom'];
    $kandytat_mieszkanie = $_POST['kandydatMieszkanie'];
    $kandytat_miejscowosc = $_POST['kandydatMiejscowosc'];
    $kandytat_kandydatKod = $_POST['kandydatKod'];
    $kandytat_kandydatSzkola = $_POST['kandydatSzkola'];
    $kandytat_kandydatKlasa = $_POST['kandydatKlasa'];
    $kandytat_kandydatKatechetaI = $_POST['kandydatKatechetaI'];
    $kandytat_kandydatKatechetaN = $_POST['kandydatKatechetaN'];
    $zglaszajacyImie = $_POST['zglaszajacyImie'];
    $zglaszajacyNazwisko = $_POST['zglaszajacyNazwisko'];
    $email = $_POST['zglaszajacyEmail'];
    $zglaszajacyTelefon = $_POST['zglaszajacyTelefon'];
    $sakrament = $_POST['sakrament'];
    if (isset($_POST['deklaracja'])) {
        $deklaracja = $_POST['deklaracja'];
    } else {
        $deklaracja = 'Nie';
    }
    $query = 'insert into sakramenty values(null, "' . $data_przeslania . '", "' . $kandytat_imie . '", "' . $kandytat_nazwisko . '", "' . $kandytat_ulica . '", "' . $kandytat_dom . '", 
	"' . $kandytat_mieszkanie . '", "' . $kandytat_miejscowosc . '", "' . $kandytat_kandydatKod . '", "' . $kandytat_kandydatSzkola . '", "' . $kandytat_kandydatKlasa . '", "' . $kandytat_kandydatKatechetaI . '",
	"' . $kandytat_kandydatKatechetaN . '", "' . $zglaszajacyImie . '", "' . $zglaszajacyNazwisko . '", "' . $email . '", "' . $zglaszajacyTelefon . '", "' . $sakrament . '", "' . $deklaracja . '")';

    $db->query($query);

    echo '<div class="alert alert-secondary wow pulse" role="alert">
            Dziękujemy za przesłanie formularza zgłoszeniowego! :)
          </div>';
}
if (isset($_POST['imie_slub']) && isset($_POST['nazwisko_slub'])) {
    $imie = $_POST['imie_slub'];
    $nazwisko = $_POST['nazwisko_slub'];
    $email = $_POST['email_slub'];
    $telefon = $_POST['telefon_slub'];
    $godzina = $_POST['godzina_slub'];
    $data = $_POST['data_slub'];

    $query = 'insert into malzenstwo values("' .  $imie . '", "' . $nazwisko . '", "' . $data . '", "' . $godzina . '", "'.$email.'", "'.$telefon.'", "TAK", null)';

    $db->query($query);

    echo '<div class="alert alert-secondary wow pulse" role="alert">
            Dziękujemy za przesłanie wiadomości. Odpowiemy najszybciej jak to możliwe! :)
          </div>';
}
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>FORMULARZE ZGŁOSZENIOWE</h1>
                <div class="divider-h wow fadeInUp"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="formularze-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="assets/img/duszpasterstwo/grupy/top.jpg" style="margin-bottom: 40px;">
                    <nav>
                        <div class="nav justify-content-center" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="chrzest-tab"
                               data-toggle="tab"
                               href="#nav-chrzest"
                               role="tab" aria-controls="nav-chrzest" aria-selected="true">CHRZEST</a>
                            <a class="nav-item nav-link" id="roczek-tab"
                               data-toggle="tab"
                               href="#nav-roczek"
                               role="tab" aria-controls="nav-roczek" aria-selected="true">ROCZEK</a>
                            <a class="nav-item nav-link" id="komunia-tab" data-toggle="tab"
                               href="#nav-komunia"
                               role="tab" aria-controls="nav-komunia" aria-selected="false">I KOMUNIA
                                ŚWIĘTA</a>
                            <a class="nav-item nav-link" id="bierzmowanie-tab" data-toggle="tab"
                               href="#nav-bierzmowanie"
                               role="tab" aria-controls="nav-bierzmowanie" aria-selected="false">BIERZMOWANIE</a>
                            <a class="nav-item nav-link" id="malzenstwo-tab" data-toggle="tab"
                               href="#nav-malzenstwo"
                               role="tab" aria-controls="nav-malzenstwo" aria-selected="false">MAŁŻEŃSTWO</a>
                            <a class="nav-item nav-link" id="intencja-tab" data-toggle="tab"
                               href="#nav-intencja"
                               role="tab" aria-controls="nav-intencja" aria-selected="false">INTENCJA ZBIOROWA</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-chrzest" role="tabpanel"
                             aria-labelledby="nav-chrzest-tab">
                            <div class="formularze-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="nav-roczek" role="tabpanel"
                             aria-labelledby="nav-roczek-tab">
                            <div class="formularze-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-komunia" role="tabpanel"
                             aria-labelledby="nav-komunia-tab">
                            <div class="formularze-content opis">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                            <div class="formularze-content">
                                <form method="POST" action="./formularze">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <h1>DANE KANDYTATA</h1>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatImieK"
                                                           placeholder="Imię kandydata" name="kandydatImie" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatNazwiskoK"
                                                           placeholder="Nazwisko kandydata" name="kandydatNazwisko"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <select id="sakramentK" name="sakrament" class="form-control">
                                                        <option value="I KOMUNIA ŚWIĘTA">I KOMUNIA ŚWIĘTA</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatUlicaK"
                                                           placeholder="Ulica" name="kandydatUlica" required>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatDomK"
                                                               placeholder="Nr domu" name="kandydatDom" required>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatMieszkanieK"
                                                               placeholder="Nr mieszkania" name="kandydatMieszkanie">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control"
                                                               id="kandydatMiejscowoscK"
                                                               placeholder="Miejscowość" name="kandydatMiejscowosc"
                                                               required>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatKodK"
                                                               placeholder="Kod pocztowy" name="kandydatKod" required>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatSzkolaK"
                                                               placeholder="Szkoła" name="kandydatSzkola" required>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatKlasaK"
                                                               placeholder="Klasa" name="kandydatKlasa" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatKatechetaIK"
                                                           placeholder="Imię katechety" name="kandydatKatechetaI"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatKatechetaNK"
                                                           placeholder="Nazwisko katechety" name="kandydatKatechetaN"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <h1>DANE OSOBY ZGŁASZAJĄCEJ</h1>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="zglaszajacyImieK"
                                                           placeholder="Imię zgłaszającego" name="zglaszajacyImie"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="zglaszajacyNazwiskoK"
                                                           placeholder="Nazwisko zgłaszającego"
                                                           name="zglaszajacyNazwisko" required/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="zglaszajacyEmailK"
                                                           placeholder="Adres email" name="zglaszajacyEmail" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="tel" class="form-control" id="zglaszajacyTelefonK"
                                                           placeholder="Numer telefonu" name="zglaszajacyTelefon"
                                                           required>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="zgodaDaneK" name="zgodaDane" required>
                                                    <label class="custom-control-label" for="zgodaDaneK">Wyrażam zgodę
                                                        na przetwarzanie danych osobowych</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="deklaracjaK" name="deklaracja" value="Tak">
                                                    <label class="custom-control-label"
                                                           for="deklaracjaK">Deklaruję...</label>
                                                </div>
                                                <button type="submit" class="btn btn-block btn-secondary"
                                                        style="margin-top: 10px;">Wyślij
                                                    formularz
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-bierzmowanie" role="tabpanel"
                             aria-labelledby="nav-bierzmowanie-tab">
                            <div class="formularze-content opis">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                            <div class="formularze-content">
                                <form method="POST" action="./formularze">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <h1>DANE KANDYTATA</h1>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatImieB"
                                                           placeholder="Imię kandydata" name="kandydatImie" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatNazwiskoB"
                                                           placeholder="Nazwisko kandydata" name="kandydatNazwisko"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <select id="sakramentB" name="sakrament" class="form-control">
                                                        <option value="BIERZMOWANIE">BIERZMOWANIE</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatUlicaB"
                                                           placeholder="Ulica" name="kandydatUlica" required>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatDomB"
                                                               placeholder="Nr domu" name="kandydatDom" required>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatMieszkanieB"
                                                               placeholder="Nr mieszkania" name="kandydatMieszkanie">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control"
                                                               id="kandydatMiejscowoscB"
                                                               placeholder="Miejscowość" name="kandydatMiejscowosc"
                                                               required>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatKodB"
                                                               placeholder="Kod pocztowy" name="kandydatKod" required>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatSzkolaB"
                                                               placeholder="Szkoła" name="kandydatSzkola" required>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <input type="text" class="form-control" id="kandydatKlasaB"
                                                               placeholder="Klasa" name="kandydatKlasa" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatKatechetaIB"
                                                           placeholder="Imię katechety" name="kandydatKatechetaI"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="kandydatKatechetaNB"
                                                           placeholder="Nazwisko katechety" name="kandydatKatechetaN"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <h1>DANE OSOBY ZGŁASZAJĄCEJ</h1>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="zglaszajacyImieB"
                                                           placeholder="Imię zgłaszającego" name="zglaszajacyImie"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="zglaszajacyNazwiskoB"
                                                           placeholder="Nazwisko zgłaszającego"
                                                           name="zglaszajacyNazwisko" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="zglaszajacyEmailB"
                                                           placeholder="Adres email" name="zglaszajacyEmail" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="tel" class="form-control" id="zglaszajacyTelefonB"
                                                           placeholder="Numer telefonu" name="zglaszajacyTelefon"
                                                           required>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="zgodaDaneB" name="zgodaDane" required>
                                                    <label class="custom-control-label" for="zgodaDaneB">Wyrażam zgodę
                                                        na przetwarzanie danych osobowych</label>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="deklaracjaB" name="deklaracja" value="Tak">
                                                    <label class="custom-control-label"
                                                           for="deklaracjaB">Deklaruję...</label>
                                                </div>
                                                <button type="submit" class="btn btn-block btn-secondary"
                                                        style="margin-top: 10px;">Wyślij
                                                    formularz
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-malzenstwo" role="tabpanel"
                             aria-labelledby="nav-malzenstwo-tab">
                            <div class="formularze-content opis">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                            <div class="formularze-content">
                                <form method="POST" action="./formularze">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <h1>WYBÓR DATY ŚLUBU</h1>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" onfocus="(this.type='date')"
                                                           id="slubData" name="data_slub" placeholder="Data">
                                                </div>
                                                <div class="form-group">
                                                    <select id="slubGodzina" name="godzina_slub" class="form-control"
                                                            style="color: #737373;">
                                                        <option value="" disabled selected>Godzina</option>
                                                        <option value="11:00">11:00</option>
                                                        <option value="12:00">12:00</option>
                                                        <option value="13:00">13:00</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <h1>DANE OSOBY ZGŁASZAJĄCEJ</h1>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="zglaszajacyImieB"
                                                           placeholder="Imię zgłaszającego" name="imie_slub"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="zglaszajacyNazwiskoB"
                                                           placeholder="Nazwisko zgłaszającego"
                                                           name="nazwisko_slub" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="zglaszajacyEmailB"
                                                           placeholder="Adres email" name="email_slub" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="tel" class="form-control" id="zglaszajacyTelefonB"
                                                           placeholder="Numer telefonu" name="telefon_slub"
                                                           required>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="zgodaDaneM" name="zgodaDane" required>
                                                    <label class="custom-control-label" for="zgodaDaneM">Wyrażam zgodę
                                                        na przetwarzanie danych osobowych</label>
                                                </div>
                                                <button type="submit" class="btn btn-block btn-secondary"
                                                        style="margin-top: 10px;">Wyślij
                                                    formularz
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-intencja" role="tabpanel"
                             aria-labelledby="nav-intencja-tab">
                            <div class="formularze-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
</article>

<?php
$pageTitle = 'Formularze - Parafia "na Górce"';
?>
</html>
<?php
include 'title.php';
?>
