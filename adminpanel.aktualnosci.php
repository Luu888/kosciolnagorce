<?php
if (@$_SESSION['admin'] == 1 && $zalogowany = 1) {
    $aktualnosci = 'select aktualnosci.* from aktualnosci order by data_aktualnosci desc;';
    $z = $db->query($aktualnosci);
    ?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeIn">
                    <h1>AKTUALNOŚCI NA STRONIE</h1>
                    <div class="divider-h"><span></span></div>
                </div>
            </div>
        </div>
    </header>
    <article>
        <section class="zgloszenia-section">
            <div class="container">
                <a class="btn btn-block btn-outline-success" style="margin: 20px 0;"
                   href="./adminpanel.dodaj_wpis/">Dodaj nowy wpis</a>
                <?php
                foreach ($z as $zg) {
                    echo '
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-4">								
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><b>Tytuł wpisu: </b>' . $zg['tytul_aktualnosci'] . '</li>
                                        <li class="list-group-item"><b>Data wstawienia: </b>' . $zg['data_aktualnosci'] . '</li>                                        
                                        <li class="list-group-item"><button class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#exampleModal">Usuń</button></li>
                                        <li class="list-group-item"><a class="btn btn-outline-warning btn-block" href="./adminpanel.edytuj_wpis/' . $zg['id_aktualnosci'] . '/">Edytuj</a></li>
                                    </ul>
                                </div>
                                <div class="col-12 col-md-8" style="padding: .75rem 1.25rem;">                      
                                    <p class="card-text"><b>Treść: </b><br>' . $zg['tresc_aktualnosci'] . '</p>
                                </div>
                            </div>   
                        </div>             
                </div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Uwaga!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Czy na pewno chcesz usunąć tę aktualność?
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-danger" href="./adminpanel.aktualnosci/' . $zg['id_aktualnosci'] . '/usun/" >Tak</a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>
                                </div>
                            </div>
                        </div>
                    </div>';
                }
                if (isset($params[1]) && $params[1] == 'usun') {
                    $query_usun = 'DELETE FROM aktualnosci where id_aktualnosci=' . $params[0];
                    $db->query($query_usun);
                    header('Location: /kosciolnagorce/adminpanel.aktualnosci/');
                }

                ?>
            </div>
        </section>
    </article>
    <?php
} else {
    include 'blad.php';
}
?>
