<?php
	if(@$_SESSION['admin']==1 && $zalogowany=1){
	$zgloszenia = 'select aktualnosci.* from aktualnosci where id_aktualnosci='.$params[0].';';
	$id = $params[0];
	$z = $db->query($zgloszenia);
	@$q_array = $z->fetch_array(MYSQLI_ASSOC);
	$tytul = $q_array['tytul_aktualnosci'];
	$tresc = $q_array['tresc_aktualnosci'];
?>

<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>EDYCJA WPISU</h1>
                <div class="divider-h"><span></span></div>
            </div>
        </div>
    </div>
</header>
<article>
    <section class="zgloszenia-section">	
        <div class="container">
			<form method="post" action=./adminpanel.edytuj_wpis/<?php echo $id;?>/dodaj/>
				<div class="form-group">
					<label for="tytul_aktualnosci"><b>Podaj tytuł wpisu</b></label>
					<input type="text" class="form-control"  name="tytul_aktualnosci" id="tytul_aktualnosci" value="<?php echo $tytul;?>" required>
				</div>
				<div class="form-group">
					<label for="tresc_aktualnosci"><b>Podaj treść wpisu</b></label>
					<textarea class="form-control" name="tresc_aktualnosci" id="tresc_aktualnosci" rows="8" required><?php echo $tresc;?></textarea>
				</div>
				<button type="submit" class="btn btn-secondary btn-block">Wstaw wpis</button>
                            
            </form>
            <?php
								
				if(isset($params[1]) && $params[1] == 'dodaj')
					{
						$data_wystawienia = date('Y-m-d H:i:s');
						$query_dodaj = 'Update aktualnosci SET data_aktualnosci = "'.$data_wystawienia.'", tytul_aktualnosci="'.$_POST['tytul_aktualnosci'].'", tresc_aktualnosci="'.$_POST['tresc_aktualnosci'].'" WHERE id_aktualnosci="'.$id.'";';
						$db->query($query_dodaj);
						header('Location: /kosciolnagorce/adminpanel.aktualnosci/');
					}

				

            ?>
        </div>
    </section>
</article>
<?php
}
else
	{
		include 'blad.php';
	}
?>
