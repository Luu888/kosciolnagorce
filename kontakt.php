<?php

$wyslano = false;

if (isset($_POST['email'])) {
    $data_wystawienia = date('Y-m-d H:i:s');
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $query = 'insert into kontakt values(null, "' . $data_wystawienia . '", "' . $name . '", "' . $email . '", "' . $message . '")';

    $db->query($query);

    echo '<div class="alert alert-secondary wow pulse" role="alert">
            Dziękujemy za przesłanie wiadomości. Odpowiemy najszybciej jak to możliwe! :)
          </div>';
}
?>

<header>
    <div class="container">
        <div class="row">
            <div class="col wow fadeInDown">
                <h1>SKONTAKTUJ SIĘ Z NAMI</h1>
                <div class="divider-h wow fadeInDown"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="kontakt-section">
        <div class="container">
            <div class="row">
                <img src="assets/img/kontakt/top.jpg" style="margin-bottom: 50px;">
                <div class="col-12 col-xl-7 kontakt-info">
                    <h2>Rzymskokatolicka Parafia Matki Boskiej Bolesnej <br> i św. Wojciecha w Opolu</h2>
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <p>
                                <i class="fas fa-phone-alt"></i><br>
                                <b>Telefon</b><br>
                                +48 736 157 099
                            </p>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <p>
                                <i class="fas fa-map-marked-alt"></i><br>
                                <b>Adres</b><br>
                                pl. Kopernika 12<br>
                                45-040 Opole
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <p>
                                <i class="fas fa-at"></i><br>
                                <b>Adres e-mail</b><br>
                                parafianagorce@gmail.com
                            </p>
                        </div>
                    </div>
                    <div style="text-align: center; margin-bottom: 40px;">
                        <div class="divider-h"><span></span></div>
                    </div>
                    <h3>Konto bankowe naszej Parafii</h3>
                    <p>
                        MBank <br>
                        <b>18 1140 2004 0000 3102 7945 8833</b> <br>
                        <u>(Tytuł wpłaty: Darowizna na cele kultu religijnego)</u>
                    </p>
                    <h4>Za wszystkie Wasze ofiary na rzecz naszej Parafii - serdeczne Bóg zapłać!</h4>
                </div>
                <div class="col-12 col-xl-5">
                    <div class="card">
                        <div class="card-body">
                            <h4>Napisz do nas! :)</h4>
                            <p>
                                Jeśli chcesz skontaktować się z nami możesz skorzystać z formularza poniżej. Postaramy
                                się odpowiedzieć na podany przez Ciebie adres mailowy najszybciej jak to możliwe.
                            </p>
                            <p style="font-size: 14px;">
                                - Konieczne jest wypełnienie wszystkich pól formularza -
                            </p>
                            <form method="post" action=./kontakt>
                                <div class="form-group">
                                    <label for="kontaktImieNazwisko"><b>Twoje imię i nazwisko</b></label>
                                    <input type="text" class="form-control" name="name" id="imie_nazw_kontaktu"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="kontaktEmail"><b>Twój adres e-mail</b></label>
                                    <input type="email" class="form-control" name="email" id="email_kontaktu" required>
                                </div>
                                <div class="form-group">
                                    <label for="kontaktWiadomosc"><b>Twoja wiadomość</b></label>
                                    <textarea class="form-control" name="message" id="tresc_kontaktu" rows="8"
                                              required></textarea>
                                </div>
                                <button type="submit" class="btn btn-secondary btn-block" data-toggle="modal"
                                        data-target="#exampleModal">Wyślij wiadomość
                                </button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>


<?php
$pageTitle = 'Kontakt - Parafia "na Górce"';
?>
</html>
<?php
include 'title.php';
?>