<?php

?>
<header>
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>HISTORIA NASZEJ PARAFII</h1>
                <div class="divider-h wow fadeInUp"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="historia-section">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <img src="assets/img/historia/1.jpg" class="img-right" alt="sw_Wojciech">
                    <p>
                        Według tradycji powstanie kościoła łączy się z upamiętnieniem pobytu i modłów św. Wojciecha
                        na
                        tym wzgórzu, gdzieś między 984 a 995 rokiem. Św. Wojciech w czasie pobytu w Opolu miał
                        głosić
                        kazania na wzgórzu poza grodem, nawracać mieszkańców okolicznych sadyb i udzielać chrztu.
                        Legenda głosi, że gdy zabrakło wody do chrztu, św. Wojciech uderzył laską o skałę i
                        wytrysnęło
                        cudowne źródełko, które zostało później ocembrowane i nazwane studnią św. Wojciecha.
                    </p>
                    <p>
                        Kazania głosił z tak wielką żarliwością, że na kamieniu zostały odciśnięte ślady jego stóp.
                        Interesującym jest fakt, że miejsce głoszenia kazań jest niemal identyczne jak w Gdańsku, a
                        studzienka to nieodłączny atrybut kultu św. Wojciecha. Legenda o kamieniu z odciśniętymi
                        stopami
                        funkcjonuje również w Trzemesznie.
                    </p>
                    <p>
                        Niemal we wszystkich miejscowościach w Polsce, w których, według tradycji przebywał św.
                        Wojciech, pozostawił po sobie obiekty sakralne ku czci Maryi.
                    </p>
                    <p>
                        Odchodząc z Opola miał ufundować kościółek pod wezwaniem Najświętszej Maryi Panny... Tak
                        zaczyna
                        się historia Kościoła pod wezwaniem Matki Boskiej Bolesnej i św. Wojciecha w Opolu.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section historia-section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInUp">
                    <p>
                        Tradycja głosi, że pierwszy kościół drewniany poświęcony św. Wojciechowi i Najświętszej
                        Maryi
                        Pannie, wzniesiono około 1000 roku. Według przekazów, w 1254 roku powstaje na wzgórzu
                        kościół
                        parafialny. W roku 1295 prawo parafialne przeniesione zostało do kościoła kolegiackiego p.w.
                        Św.
                        Krzyża. Łączy się to prawdopodobnie z fundacją klasztoru dominikańskiego, który z woli
                        Księcia
                        tutaj zostaje osadzony, a konwent uzyskuje kościół na wzgórzu.
                    </p>
                    <p>
                        Inny przekaz głosi, że kościół dopiero w 1304 roku przekazano dominikanom, którym Bolko I w
                        1301
                        roku ufundował drewniany klasztor. Zapewne sprowadzeni bracia otrzymują istniejący kościół w
                        użytkowanie, co łączy się z przeniesieniem prawa parafialnego na kościół kolegiacki.
                    </p>
                    <p>
                        W połowie XIV wieku wzniesione zostają murowane budynki kościoła i klasztoru. W 1361 roku
                        kościół zostaje poświęcony Najświętszej Maryi Pannie, św. Annie i świętym: Wojciechowi,
                        Jerzemu
                        i Dominikowi. Przed rokiem 1430 w kościele przeprowadzono prace budowlane na tyle istotne,
                        że
                        nastąpiła ponowna konsekracja kościoła. W 1480 roku następuje ponowne poświęcenie kościoła,
                        jednak powód tego nie jest znany. W roku 1530 dominikanie opuszczają Opole.
                    </p>
                    <img src="assets/img/historia/2.jpg" class="img-center" alt="kosciol">
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <img src="assets/img/historia/3.jpg" class="img-left" alt="kosciol">
                    <p>
                        Inwentarz kościelny przejmuje Magistrat. W 1540 roku protestancki margrabia uzyskuje
                        pozwolenie
                        na korzystanie z kościoła. W roku 1558 kościół nakazem cesarskim zostaje zamknięty. W latach
                        1566 i 1572 wzmiankowane jest znaczne zaniedbanie kościoła.
                    </p>
                    <p>
                        W roku 1604 dominikanie ponownie przejmują kościół. W 1614 roku przystąpiono do odbudowy
                        zniszczonego kościoła i klasztoru. Wtedy to wzniesiono nowy szczyt zachodni kościoła „Na
                        Górce”.
                        Pożar w 1615 r. przerywa prowadzone prace, które ostatecznie zakończono w 1617 roku
                        położeniem
                        nowego dachu. Pięć lat później w mieście wybucha kolejny pożar. Od tego czasu odbywano
                        procesje
                        błagalne z kościoła Św. Krzyża do kościoła Matki Bożej na Górce. Tutaj dominikanie
                        odprawiali
                        nabożeństwa o uproszenie Boga, by zachował Opole od takich klęsk.
                    </p>
                    <p>
                        Najazd Szwedów na miasto w 1634 r. spowodował ogromne zniszczenia kościoła i klasztoru. Z
                        tymi
                        wydarzeniami wiąże się także ukrycie jasnogórskiego obrazu w klasztorze Pauliny (obecnie
                        Mochów)
                        koło Głogówka. Po zakończeniu wojny ze Szwedami, obraz wracał do Częstochowy i w drodze tej,
                        w
                        Wielkim Tygodniu 1656 r., zatrzymał się w Opolu, skąd w uroczystej asyście odwieziono go na
                        Jasną Górę. Potop szwedzki dobitnie zniszczył kościół i klasztor a następne lata przyniosły
                        szereg pożarów. W 1682 wskutek jednego z nich, kościół został znacznie zniszczony, lecz
                        dzięki
                        dotacjom, został odbudowany przez zakonników. Prace trwały od 1701 do 1708 roku. Nastąpiła
                        gruntowna przebudowa kościoła: ponowne zasklepienie jego wnętrz i jego barokizacja. W
                        kolejnym
                        pożarze miasta, w roku 1739 zniszczony zostaje kościół i klasztor. Odbudowany ze składek
                        wiernych ponownie zostaje dotknięty pożarem w roku 1762. Dzięki wysiłkom ówczesnego przeora
                        Jerzego Langa usunięto zniszczenia. Odnowiony kościół przetrwał do 1810 roku.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section historia-section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInRight">
                    <img src="assets/img/historia/4.jpg" class="img-right" alt="kosciol">
                    <p>
                        Po sekularyzacji w 1810r., oszacowano wartość kościoła i w 1811 r. oderwano go od klasztoru
                        oraz
                        przekazano pobliskiemu gimnazjum katolickiemu. Zakrystia, która była częścią klasztoru,
                        została
                        oddzielona od kościoła murem. W kościele, w miejsce obrazu Matki Bożej Różańcowej, wstawiono
                        łaskami słynący, obraz Matki Bożej Piekarskiej.
                    </p>
                    <p>
                        W czasie kampanii napoleońskiej, świątynię zamieniono w magazyn wojskowy a następnie, w
                        szpital.
                        Rektor gimnazjum – Glőger, przeniósł nabożeństwa do kaplicy gimnazjalnej a z kościoła
                        usunięto
                        obraz Matki Bożej Piekarskiej.
                    </p>
                    <p>
                        W 1820 r. kościół na nowo przekazano wspólnocie katolickiej a kuratelę nad świątynią oddano
                        w
                        ręce ostatniego przeora dominikańskiego, o. A. Mondrego.
                    </p>
                    <p>
                        Pod koniec XIX wieku postanowiono wznieść wieżę przy bocznej kaplicy. Zamiar ten
                        zrealizowano w
                        latach 1895 – 1896. Na górnym piętrze ośmiobocznej barokowej wieży ( wysokość 42 metry )
                        umieszczono dzwonnicę.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <img src="assets/img/historia/5.jpg" class="img-left" alt="kosciol">
                    <p>
                        W 1912 wykonano bezpośrednie dojście z zewnątrz do zakrystii (obecnie jest to kaplica Matki
                        Bożej Częstochowskiej). W 1926 r. pokryto na nowo dach kościoła i zniesiono wieżyczkę nad
                        zachodnim szczytem. W latach 1931-38 przeprowadzono ponownie gruntowny remont: wyburzono
                        schody
                        zachodnie i wzniesiono w całkiem innym kształcie nowe. Wtedy, przed wejściem głównym,
                        usunięto
                        stary krzyż misyjny i figurę Jana Nepomucena.
                    </p>
                    <p>
                        Świątynia uzyskała nowy wygląd. Odnowiono ołtarze, wykonano polichromię i złocenia, usunięto
                        ołtarz główny w stylu romańskim i zastąpiono go nowym, istniejącym do dziś. W płycie
                        ołtarzowej
                        znajdują się relikwie świętych męczenników: Mangusa i Sewery. Nad ołtarzem umieszczono obraz
                        przedstawiający chwałę świętych – Wojciecha i Jerzego. Obraz jest już wzmiankowany w roku
                        1784.
                    </p>
                    <p>
                        Od maja 1934 r. kościół Maryjny stał się samodzielną jednostką duszpasterską. Parafia
                        Najświętszej Maryi Panny w Opolu, początkowo pozostawała pod kuratelą parafii Św. Krzyża,
                        jednak
                        od 1 stycznia 1940 r. została zatwierdzona przez władze państwowe.
                    </p>
                    <p>
                        Po II wojnie światowej utrzymano parafię przy kościele Maryjnym, która skupiała przede
                        wszystkim
                        repatriantów ze Wschodu przybyłych do Opola. Pierwszym proboszczem powojennym został ks. E.
                        Kobierzycki.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section historia-section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInRight">
                    <img src="assets/img/historia/6.jpg" class="img-right" alt="kosciol">
                    <p>
                        Sama świątynia, wskutek działań wojennych nieco ucierpiała, dlatego trzeba było ją
                        remontować.
                        Wnętrze kościoła odnowiono w roku 1950 i stał się on, w powszechnej opinii, najpiękniejszym
                        kościołem Opola.W 1951 r. nowym proboszczem parafii na Górce zostaje ks. K. Borcz. Za jego
                        czasów przebudowano kaplicę Matki Bożej Częstochowskiej, spełniającą wtedy rolę zakrystii i
                        przywrócono jej dawną funkcję.
                    </p>
                    <p>
                        W 1957 roku parafia złożyła Śluby Jasnogórskie. Główna uroczystość odbyła się 5 maja. W 1965
                        r.
                        przeżyliśmy jako wspólnota bardzo podniosłą uroczystość – nawiedzenie obrazu Matki Bożej
                        Częstochowskiej w Wielkim Tygodniu. Była to już druga peregrynacja.
                    </p>
                    <p>
                        W roku 1972 proboszczem zostaje ks. Edward Kucharz (1931-2006), który pełni tę posługę do
                        końca
                        swojego życia. Jego nagła śmierć w marcowe popołudnie na schodach wiodących do kościoła na
                        „Górce”, była wstrząsem dla ludzi z śródmieścia, bowiem ks. Kucharz był powszechnie znany. W
                        powojennym Opolu bardzo przysłużył się kościołowi na Górce. Z wykształcenia doktor historii
                        sztuki po Katolickim Uniwersytecie Lubelskim ale również studiował konserwację dzieł sztuki
                        na
                        krakowskiej Akademii Sztuk Pięknych. Był zatem teologiem, historykiem sztuki jak i wielkim
                        miłośnikiem zabytków oraz ich konserwatorem. Świątynię na „Górce” traktował jak dom
                        rodzinny,
                        walczył o jej przetrwanie w czasach, kiedy poważne pęknięcie skarpy na opolskim wzgórzu
                        groziło
                        kościołowi zawaleniem. Zdarzały się sytuacje, że podczas nabożeństw, na głowy wiernych sypał
                        się
                        tynk. Nie wolno było używać dzwonu na kościelnej wieży w obawie, by drgania wywołane
                        akustyczną
                        falą nie przyśpieszyły procesu pękania murów.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <img src="assets/img/historia/7.jpg" class="img-left" alt="wieza_koscielna">
                    <p>
                        Sama świątynia, wskutek działań wojennych nieco ucierpiała, dlatego trzeba było ją
                        remontować.
                        Wnętrze kościoła odnowiono w roku 1950 i stał się on, w powszechnej opinii, najpiękniejszym
                        kościołem Opola. W 1951 r. nowym proboszczem parafii na Górce zostaje ks. K. Borcz. Za jego
                        czasów przebudowano kaplicę Matki Bożej Częstochowskiej, spełniającą wtedy rolę zakrystii i
                        przywrócono jej dawną funkcję.
                    </p>
                    <p>
                        W 1957 roku parafia złożyła Śluby Jasnogórskie. Główna uroczystość odbyła się 5 maja. W 1965
                        r.
                        przeżyliśmy jako wspólnota bardzo podniosłą uroczystość – nawiedzenie obrazu Matki Bożej
                        Częstochowskiej w Wielkim Tygodniu. Była to już druga peregrynacja.
                    </p>
                    <p>
                        Od 2006 r. proboszczem parafii jest ks. Marek Trzeciak. W latach 1985-1988 oraz 1995-2005
                        poświęcił się służbie misyjnej, niosąc Dobrą Nowinę mieszkańcom peruwiańskich Andów. Po
                        powrocie
                        został przydzielony do parafii Matki Boskiej Bolesnej i św. Wojciecha na statusie rezydenta.
                        Wkrótce, po śmierci ks. E. Kucharza, został mianowany Ojcem parafii. Ciepło przyjęty przez
                        parafian rozpoczął pracę duszpasterską ale również podjął starania by wyremontować kościół,
                        który od początku z dumą nazywa „opolską perełką”.
                    </p>
                    <p>
                        W latach 2007 – 2010 odnowiona została wieża i część elewacji: wschodnia i północna. Koszt
                        tych
                        prac został pokryty z ofiar parafian (kolekty na remont kościoła, cegiełki), sponsorów oraz
                        dotacji Urzędu Marszałkowskiego i Urzędu Miasta Opole. Dalszy remont, ze względu na koszty,
                        stanął pod znakiem zapytania.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section historia-section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInUp">
                    <p>
                        Z pomocą przyszedł Program Rewitalizacji Miasta. Jako jeden z najważniejszych zabytków w
                        Opolu,
                        kościół mógł się wpisać w realizację tego programu. Opracowany program okazał się najlepszy
                        i
                        uzyskał dotację w ramach Regionalnego Programu Operacyjnego Województwa. Po blisko rocznym
                        okresie przygotowań formalnych, w czerwcu 2011 roku rusza drugi etap remontu, który
                        obejmuje:
                        remont dachu, dokończenie remontu elewacji, remont organów i zmianę ogrzewania w kościele.
                        Od
                        listopada można podziwiać nowy dach. Remont organów rozpoczął się 30 listopada 2011r.
                        „Opolska
                        Perełka” pięknieje nie tylko na zewnątrz.
                    </p>
                    <p>
                        22 października 2011 roku w kościele została odprawiona uroczysta Msza Święta z Intronizacją
                        Relikwii bł. Jana Pawła II. Mszy Św. przewodniczył i kazanie wygłosił ks. bp Jan Kopiec.
                    </p>
                    <p>
                        Trzeciego grudnia ks. proboszcz Marek Trzeciak poświęcił miejsce stałej ekspozycji relikwii
                        przy
                        kaplicy Matki Bożej. Jest to klęcznik, u którego na pulpicie, pod szybą zostały umieszczone
                        relikwie. Nad klęcznikiem zawisł podświetlony obraz bł. Jana Pawła II. Od teraz, o każdej
                        porze
                        dnia, można oddać cześć błogosławionemu Ojcu Świętemu.
                    </p>
                    <img src="assets/img/historia/9.jpg" class="img-center" alt="remont_elewacji">
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInUp">
                    <p>
                        23 kwietnia 2013, w dzień świętego Wojciecha, na uroczystej Sumie odpustowej, której
                        przewodniczył ks. biskup Andrzej Czaja, został oficjalnie zakończony remont naszego
                        Kościoła.
                        Mszą Świętą, nasza wspólnota parafialna podziękowała Bogu i ludziom za pomyślne zakończenie
                        remontu. Ksiądz biskup poświęcił odnowione organy a my, mogliśmy je usłyszeć w pełnym
                        brzmieniu
                        na Koncercie, w dniu 28 kwietnia, w którym wystąpił pan prof. Krzysztof Latała oraz chór
                        parafialny ExultateDeo, pod dyrekcją pana Tomasza Krzemińskiego.
                    </p>
                    <p>
                        Piątego i szóstego marca 2015 roku w ramach wizytacji kanonicznej, gościliśmy w naszej
                        Parafii
                        ks. biskupa Andrzeja Czaję. Jesienią 2016 roku rozpoczął się remont schodów. Zakończenie
                        planowane jest na wiosnę 2017.
                    </p>
                    <p>
                        25 marca 2017 rozpoczęła się w naszym kościele Dominikańska Szkoła Wiary: cykl wykładów na
                        tematy teologiczne, etyczne, filozoficzne, życia społecznego. Organizatorem DSW jest
                        działająca
                        przy Parafii Matki Boskiej Bolesnej i Św. Wojciecha grupa świeckich Dominikanów i
                        wolontariuszy
                        oraz ks. proboszcz Marek Trzeciak. Pierwszy wykład pt. „Przekazywać owoce kontemplacji –
                        źródła
                        tożsamości dominikańskiej” wygłosił o. Marek Miławicki OP – teolog i historyk, członek
                        Dominikańskiego Instytutu historycznego w Krakowie, który zajmuje się nowożytną i
                        współczesną
                        historią Zakonu Dominikańskiego.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4 historia-section wow fadeInUp">
                    <img src="assets/img/historia/10.jpg" class="img-center" alt="wizyta_biskupa">
                </div>
                <div class="col-12 col-lg-4 historia-section wow fadeInUp">
                    <img src="assets/img/historia/11.jpg" class="img-center" alt="kosciol_po_remoncie">
                </div>
                <div class="col-12 col-lg-4 historia-section wow fadeInUp">
                    <img src="assets/img/historia/12.jpg" class="img-center"
                         alt="dominikanska_szkola_wiary">
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section historia-section-gray-bg">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <img src="assets/img/historia/13.jpg" class="img-left" alt="about">
                    <p>
                        Ze względów zdrowotnych, po przeszło trzynastu latach, ks. Marek Trzeciak poprosił księdza
                        biskupa Andrzeja Czaję o odwołanie z urzędu proboszcza Parafii Matki Boskiej Bolesnej i św.
                        Wojciecha w Opolu. Ksiądz biskup przyjął rezygnację i mianował ks. Marka rezydentem w
                        Parafii
                        Matki Bożej Wspomożenia Wiernych w Kluczborku.
                    </p>
                    <p>
                        Nowym proboszczem Biskup Opolski mianował ks. Bogusława Króla, dotychczasowego wikariusza w
                        Parafii Najświętszego Serca Pana Jezusa w Kluczborku. Zmiana weszła w życie z dniem 28
                        sierpnia
                        2019. Msza Święta pożegnalna ks. Marka odbyła się 25 sierpnia o godzinie 10.30.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="historia-section">
        <div class="container">
            <div class="row">
                <div class="col wow fadeInLeft">
                    <p>
                        Bibliografia <br>
                        1. Władysław Dziewulski, Franciszek Hawranek: „Opole, monografia miasta”;
                        Instytut Śląski, Opole 1975r.<br>
                        2. Urszula Popłonyk: „Opole”; Ossolineum, Wrocław – Warszawa – Kraków 1970<br>
                        3. Franciszek Idziowski: „Opole, dzieje miasta do 1863 roku”; Wydział Teologiczny
                        Uniwersytetu Opolskiego, Opole 2002r.<br>
                        4. Anna Pobóg – Lenartowicz: „Święty Wojciech”; Wydawnictwo WAM, Kraków 2002r.<br>
                        5. Ks. Kazimierz Śmigiel: „Święty Wojciech w tradycji i kulturze europejskiej”; Prymasowe
                        Wydawnictwo Gaudentinum, Gniezno 1992r.<br>
                        6. Ks. Stanisław Klein, ks. Józef Urban: „Idźcie na cały świat ...”, Kapłani Diecezji
                        Opolskiej na kontynentach świata; Wydział Teologiczny Uniwersytetu Opolskiego,
                        Opole 2001r.<br>
                        7. Ryszard Emmerling: „Opole, cztery pory roku”; Śląskie Wydawnictwo ADAN, Opole
                        2005r.<br>
                        8. Stanisław Sławomir Nicieja: „Wzgórze Uniwersyteckie w Opolu”; Wydawnictwo mS,
                        Opole 2008r.<br>
                        9. Ks. Edward Kucharz : „Opolski Kościół na Górce”; Wydawnictwo Św. Krzyża, Opole 1998
                    </p>
                </div>
            </div>
        </div>
    </section>

</article>
<?php
$pageTitle = 'Historia - Parafia "na Górce"';
?>
</html>
<?php
include 'title.php';
?>