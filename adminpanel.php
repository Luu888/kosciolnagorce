<?php
if (@$_SESSION['admin'] == 1 && $zalogowany == 1) {
    $pozycje_menu = array(array('alias' => 'zgloszenia', 'nazwa' => 'Zgłoszenia'), array('alias' => 'aktualnosci', 'nazwa' => 'Aktualności'), array('alias' => 'galeria', 'nazwa' => 'Galeria'), array('alias' => 'sakramenty', 'nazwa' => 'Sakramenty'), array('alias' => 'adoracje', 'nazwa' => 'Adoracje'), array('alias' => 'malzenstwo', 'nazwa' => 'Małżeństwo'));
    if (!empty($params[0])) {
        $active = array_shift($params);
    } else {
        $active = 'zgloszenia';
    }
    ?>
    <article style="margin: 30px 0;">
        <div class="container">

            <?php
            foreach ($pozycje_menu as $men) {
                echo '<a class="btn btn-outline-dark" 
                     href="./adminpanel/' . $men['alias'] . '"
                     style="margin-bottom: 5px;"> 
                     ' . $men['nazwa'] . ' 
                    </a>&nbsp;';
            }
            ?>
            <div style="margin-top: 30px;">
                <?php include 'adminpanel.' . $active . '.php'; ?>
            </div>
        </div>
    </article>
    <?php
} else {
    include 'blad.php';
}

?>
