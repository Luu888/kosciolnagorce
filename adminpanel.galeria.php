<?php
function deleteDirectory($dir)
{
    if (!file_exists($dir)) {
        return true;
    }

    if (!is_dir($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }

        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }

    }

    return rmdir($dir);
}

function url_decode($string)
{
    return utf8_decode(urldecode($string));
}

if (@$_SESSION['admin'] == 1 && $zalogowany = 1) {
    ?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeIn">
                    <h1>ZARZĄDZANIE GALERIĄ</h1>
                    <div class="divider-h"><span></span></div>
                </div>
            </div>
        </div>
    </header>
    <article>
        <section class="zgloszenia-section">
            <div class="container">
                <a class="btn btn-outline-success btn-block" style="margin: 20px 0;" href="./adminpanel.galeria.dodaj/">Stwórz
                    nową galerię</a>
                <div class="card-columns">
                    <?php
                    $path = 'gallery/';
                    $results = scandir($path);

                    foreach ($results as $result) {
                        if ($result === '.' or $result === '..') continue;

                        if (is_dir($path . $result)) {
                            echo '<div class="card">
                                    <div class="card-body">
                                        <div class="card-text galeria">
                                            <a href="galeria.wyswietl/' . $result . '/">' . $result . '</a><br>
                                        </div>
                                        <button class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#exampleModal">Usuń Galerię</button>                                    
                                        
                                        <a class="btn btn-outline-success btn-block" href="./adminpanel.galeria.edytuj/' . $result . '/">Edytuj Galerię</a>
                                    </div>
                                   </div>
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Uwaga!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Czy na pewno chcesz usunąć tę galerię?
                                                </div>
                                                <div class="modal-footer">
                                                    <a type="button" class="btn btn-danger" href="./adminpanel.galeria/' . $result . '/usun/">Tak</a>                                                    
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>                                                        
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
                        }
                    }
                    if (isset($params[1]) && $params[1] == 'usun') {
//                        echo $path . $params[0];
                        deleteDirectory($path . urldecode($params[0]));
                        header('Location: /kosciolnagorce/adminpanel.galeria');
                    }
                    ?>
                </div>
            </div>
        </section>
    </article>
    <?php
} else {
    include 'blad.php';
}
?>