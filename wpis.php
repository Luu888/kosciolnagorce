<?php
$wyswietl = 'select aktualnosci.* from aktualnosci where id_aktualnosci=' . $params[0];
$z = $db->query($wyswietl);
$id = $params[0];
$q_array = $z->fetch_array(MYSQLI_ASSOC);
$tytul = $q_array['tytul_aktualnosci'];
$tresc = $q_array['tresc_aktualnosci'];
$data = $q_array['data_aktualnosci'];
?>

<header>
    <div class="container">
        <div class="row">
            <div class="col wow fadeIn">
                <?php echo '<h1>' . $tytul . '</h1>'; ?>
                <div class="divider-h"><span></span></div>
                <?php
                echo '
                    <p class="data-wpis">
                        DODANO: ' . $data . '
                    </p>
                '
                ?>

            </div>
        </div>
    </div>
</header>

<article>
    <section class="aktualnosc-section">
        <div class="container">
            <div class="aktualnosc-content">
                <?php
                    echo $tresc;
                ?>
            </div>
        </div>
    </section>
</article>





