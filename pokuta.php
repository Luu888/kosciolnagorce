<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>POKUTA</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                Weźmijcie Ducha Świętego! Którym odpuścicie grzechy są im odpuszczone, a którym
                                zatrzymacie, są im zatrzymane
                            </p>
                            J 20,23
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/pokuta.jpg" style="margin-bottom: 30px;">
                        <div class="sakramenty-content">
                            <p>
                                Jeżeli chcesz doświadczyć uzdrawiającej łaski odpuszczenia zła, które masz na sumieniu,
                                zapraszamy do skorzystania z sakramentu pokuty w godzinach:
                            </p>
                            <p>
                                <b>Poniedziałek, Wtorek, Czwartek, Piątek</b><br>
                                6.10 - 6.25 | 17.30 - 17.45<br>
                                <b>Środa</b><br>
                                6.10 - 6.25<br>
                                <b>Sobota</b><br>
                                6.10 - 6.25 | 17.00 - 17.45<br>
                                <b>Niedziela</b><br>
                                pół godziny przed każdą mszą świętą
                            </p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="sakramenty-cytat" style="margin-top: 30px;">
                            <p>
                                Jestem hojniejszy dla grzeszników niżeli dla sprawiedliwych. Dla nich zstąpiłem na
                                ziemię…
                                dla nich przelałem krew, niech się nie lękają do mnie zbliżyć, oni najwięcej
                                potrzebują
                                mojego miłosierdzia
                            </p>
                            Słowa Jezusa zapisane w Dzienniczku św. Faustyny
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Pokuta - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>