<?php

?>
<header>
    <div class="container">
        <div class="row">
            <div class="col wow fadeInDown">
                <h1>GRUPY PARAFIALNE</h1>
                <div class="divider-h wow fadeInDown"><span></span></div>
            </div>
        </div>
    </div>
</header>

<article>
    <section class="grupy-section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <img src="assets/img/duszpasterstwo/grupy/top.jpg" style="margin-bottom: 40px;">
                    <nav>
                        <div class="nav justify-content-center" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="krag-biblijny-tab" data-toggle="tab"
                               href="#nav-krag-biblijny"
                               role="tab" aria-controls="nav-krag-biblijny" aria-selected="false">KRĄG BIBLIJNY</a>
                            <a class="nav-item nav-link" id="caritas-tab" data-toggle="tab"
                               href="#nav-caritas"
                               role="tab" aria-controls="nav-caritas" aria-selected="false">CARITAS</a>
                            <a class="nav-item nav-link" id="lso-tab" data-toggle="tab"
                               href="#nav-lso"
                               role="tab" aria-controls="nav-lso" aria-selected="false">LITURGICZNA SŁUŻBA OŁTARZA</a>
                            <a class="nav-item nav-link" id="dzieci-maryi-tab" data-toggle="tab"
                               href="#nav-dzieci-maryi"
                               role="tab" aria-controls="nav-dzieci-maryi" aria-selected="false">DZIECI MARYI</a>
                            <a class="nav-item nav-link" id="chor-parafialny-tab"
                               data-toggle="tab"
                               href="#nav-chor-parafialny"
                               role="tab" aria-controls="nav-chor-parafialny" aria-selected="false">CHÓR PARAFIALNY</a>
                            <a class="nav-item nav-link" id="dsw-tab" data-toggle="tab"
                               href="#nav-dsw"
                               role="tab" aria-controls="nav-dsw" aria-selected="false">DOMINIKAŃSKA SZKOŁA WIARY</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-krag-biblijny" role="tabpanel"
                             aria-labelledby="nav-krag-biblijny-tab">
                            <div class="row">
                                <div class="col-12 col-xl-4 img"
                                     style="background-image: url('assets/img/duszpasterstwo/grupy/krag.jpg');">
                                </div>
                                <div class="col-12 col-xl-8">
                                    <div class="grupy-content">
                                        <h2>KRĄG BIBLIJNY</h2>
                                        <p>
                                            Krąg Biblijny to formuła spotkań otwartych dla każdego. Podczas każdego
                                            spotkania kręgu
                                            czytamy Fragmenty Pisma świętego następującej niedzieli i próbujemy
                                            wydobywać z nich
                                            przesłanie, które kieruje do nas Pan Bóg. Jeżeli chciałbyś zdobywać
                                            umiejętność
                                            słuchania Bożego słowa, zapraszamy Cię na spotkania Kręgu Biblijnego.
                                        </p>
                                        <p>
                                            Zazwyczaj odbywają
                                            się one w czwartki po wieczornej mszy świętej czyli ok godz. 18.45 w salce
                                            parafialnej.
                                            W związku z panującą epidemią spotkania są zawieszone. Informację o tym, czy
                                            w bieżącym
                                            tygodniu jest spotkanie znajdziesz w ogłoszeniach parafialnych.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-caritas" role="tabpanel"
                             aria-labelledby="nav-caritas-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="grupy-content">
                                        <h1>CARITAS PARAFIALNA</h1>
                                        <p>
                                            Wszelkie sprawy dotyczące pomocy najuboższym w naszej parafii można zgłaszać
                                            w Parafialnej Caritas
                                            w każdą środę w godz. 16:00 – 17:00
                                        </p>
                                        <p>
                                            <b>PARAFIALNA CARITAS - NIECZYNNA DO ODWOŁANIA</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-lso" role="tabpanel"
                             aria-labelledby="nav-lso-tab">
                            <div class="grupy-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-dzieci-maryi" role="tabpanel"
                             aria-labelledby="nav-dzieci-maryi-tab">
                            <div class="grupy-content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus augue nec
                                egestas congue. Nam posuere tincidunt auctor. Donec scelerisque arcu sit amet
                                velit fringilla egestas. Nullam nec nunc accumsan, cursus nunc vitae, gravida
                                ex. Cras fermentum, lacus sit amet porta interdum, purus orci eleifend neque,
                                non dapibus eros elit in quam. Duis commodo mauris a elit sollicitudin
                                tincidunt. Nam faucibus odio id leo ultrices ornare
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-chor-parafialny" role="tabpanel"
                             aria-labelledby="nav-chor-parafialny-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="grupy-content">
                                        <h2>CHÓR PARAFIALNY</h2>
                                        <p>
                                            Chór kameralny <b>„Exultate Deo”</b> powstał wiosną 2007 roku, skupiając
                                            nieformalnie
                                            osoby, w jakiś sposób związane z historią kościoła „Na Górce”, będące
                                            jednocześnie entuzjastami śpiewania bądź to zespołowego, bądź też
                                            chóralnego.
                                            Inspiratorami założenia chóru są ks. proboszcz Marek Trzeciak, ks. Andrzej
                                            Demitrów – ówczesny wikariusz naszej parafii oraz członkowie dawnego Zespołu
                                            Wokalnego, działającego przy parafii Św. Ap. Piotra i Pawła w Opolu, kiedy
                                            ks.
                                            Marek Trzeciak był tam wikarym. Repertuar chóru związany jest przede
                                            wszystkim z
                                            potrzebami liturgii, lecz muzycznie sięga do różnych epok i stylów.
                                        </p>
                                        <p>
                                            Dyrygentem chóru jest Tomasz Krzemiński, od wielu lat prowadzący kościelne
                                            zespoły
                                            śpiewacze, w przeszłości współpracujący z Filharmonią Opolską oraz Teatrem
                                            im.
                                            J. Kochanowskiego w Opolu. W roku 2008, dzięki dobrej woli wielu osób,
                                            powstaje
                                            „płyta cegiełka” - <b>„Maria Regina Mundi”</b>. Na przestrzeni wieków,
                                            świątynia
                                            wielokrotnie ulegała zniszczeniom. Za każdym razem jednak była podnoszona
                                            przez
                                            kolejne pokolenia. Niedawno ratowaliśmy nasz kościół zagrożony zawaleniem.
                                            Dzisiaj jest już bezpieczny, co potwierdziły niedawne ekspertyzy, ale wymaga
                                            kosztownej renowacji. Ta płyta jest próbą pomocy w tym dziele. Niech to
                                            będzie
                                            nasz historyczny wkład w to miejsce, gdzie od ponad dziesięciu wieków
                                            chwalone
                                            jest Imię Pana...
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-dsw" role="tabpanel"
                             aria-labelledby="nav-dsw-tab">
                            <div class="row">
                                <div class="col-12 col-xl-8">
                                    <div class="grupy-content">
                                        <h2>DOMINIKAŃSKA SZKOŁA WIARY</h2>
                                        <p>
                                            25 marca 2017 r. w kościele Parafii Matki Boskiej Bolesnej i Św. Wojciecha,
                                            rozpoczęła się Dominikańska Szkoła Wiary: cykl wykładów na tematy
                                            teologiczne, etyczne, filozoficzne, życia społecznego.
                                        </p>
                                        <p>
                                            Patronat nad Dominikańską Szkołą Wiary w Opolu objął Ksiądz Biskup Andrzej
                                            Czaja oraz "Gość Niedzielny" i Radio Doxa.
                                        </p>
                                        <p>
                                            Bieżące informacje o wydarzeniach związanych z DSW będą zamieszczane
                                            <a href="https://www.facebook.com/dominikanie.opole">tutaj</a>.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-12 col-xl-4 img"
                                     style="background-image: url('assets/img/duszpasterstwo/grupy/dsw.png');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</article>

<?php
$pageTitle = 'Grupy Parafialne - Parafia "na Górce"';
?>
</html>
<?php
include 'title.php';
?>
