<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>EWANGELIZACJA</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="ewangelizacja-section">
            <div class="container">
                <div class="row">
                    <div class="col-12" style="padding: 0">
                        <div class="ewangelizacja-cytat">
                            <p>
                                Żniwo wprawdzie wielkie, ale robotników mało; proście więc Pana żniwa, żeby wyprawił
                                robotników na swoje żniwo
                            </p>
                            Łk 10,2
                        </div>
                    </div>
                    <div class="col-12 col-lg-8 left">
                        <div class="ewangelizacja-content">
                            <h2>EWANGELIZACJA</h2>
                            <p>
                                Parafia której stronę odwiedzasz jest pierwszym w historii Opola miejscem skąd
                                rozpoczęło się głoszenie Zwycięstwa Chrystusa nad śmiercią. Na wzgórzu na którym stoi
                                nasz kościół święty Wojciech w końcówce X w. rozpoczął ewangelizację plemion żyjących w
                                tej okolicy. Pomimo upływu ponad 1000 lat od misji prowadzonej przez jednego z
                                największych ewangelizatorów w historii kościoła, próbujemy kontynuować jego dzieło.
                                Jeżeli Ty także masz pragnienie aby, doświadczenie Boga, które nosisz w sercu dotarło do
                                jak największej ilości osób zapraszamy Cię do współpracy.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 img"
                         style="background-image: url('assets/img/ewangelizacja/ewangelizacja.jpg');">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-4 img"
                         style="background-image: url('assets/img/ewangelizacja/modlitwa.jpg');">
                    </div>
                    <div class="col-12 col-lg-8 right">
                        <div class="ewangelizacja-content">
                            <h2>MODLITWA</h2>
                            <p>
                                Przede wszystkim zapraszamy Cię do wspólnej modlitwy o dar ewangelizacji Opola.
                                codziennie Adorujemy Chrystusa obecnego w Najświętszym sakramencie prosząc aby Jego
                                Miłość docierała do wszystkich, którzy jej potrzebują. Jeżeli chcesz się przyłączyć do
                                Adoracji w intencji Ewangelizacji Opola skorzystaj z dostępnego formularza, przyjdź do
                                naszego kościoła i módl się o zwycięstwo Bożego miłosierdzia w sercach wszystkich ludzi.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-8 left">
                        <div class="ewangelizacja-content">
                            <h2>POST</h2>
                            <p>
                                Głoszenie Ewangelii, które było zasadniczym celem misji Chrystusa zostało poprzedzone
                                Jego czterdziestodniowym postem na pustyni, podobnie apostołowie przed rozpoczęciem
                                misji podejmowali w jej intencji post. Dlatego próbując naśladować powyższe wzory
                                zachęcamy Cię do podjęcia dobrowolnego umartwienia, które ofiarowane w intencji
                                Ewangelizacji i złączone z męką Chrystusa na krzyżu będzie ziarnem przynoszącym owoc
                                Nowego życia w sercach tych do których idziemy z dobrą Nowiną.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 img"
                         style="background-image: url('assets/img/ewangelizacja/post.jpg');">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-4 img"
                         style="background-image: url('assets/img/ewangelizacja/jalmuzna.jpg');">
                    </div>
                    <div class="col-12 col-lg-8 right">
                        <div class="ewangelizacja-content">
                            <h2>JAŁMUŻNA</h2>
                            <p>
                                Każde dzieło prowadzone przez człowieka potrzebuje przynajmniej minimalnych środków
                                materialnych. Najważniejszym „środkiem” Ewangelizacyjnym jest zawsze człowiek -
                                ewangelizator, gotowy poświęcić swój czas, talenty i energię. Jeżeli więc chciałbyś
                                swoją pracą przyczynić się do rozwoju Misji Chrystusa, zapraszamy Cię do naszego
                                wolontariatu. Jeżeli nie masz możliwości aby złożyć jałmużnę własnej obecności ale
                                chciałbyś przyczynić się do Ewangelizacji możesz również złożyć ofiarę pieniężną która
                                na ten cel zostanie przeznaczona.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 center">
                        <div class="ewangelizacja-content">
                            <h2>WOLONTARIAT</h2>
                            <p>
                                W dzisiejszym świecie często skoncentrowanym na osiąganych zyskach chcemy Ci
                                zaproponować posługę niedającą żadnych korzyści. Jedyne co osiągniesz to świadectwo
                                sumienia, że wykonałeś coś zupełnie bezinteresownie. można taką posługę pełnić w
                                niezliczonych codziennych sytuacjach.
                            </p>
                            <p>
                                Dlaczego proponujemy nasz wolontariat? Nasza świątynia będąca najstarszą sakralną
                                budowlą Opola stoi dzięki ludziom, którzy nie
                                licząc na własne korzyści pragnęli troszczyć się o piękno wskazujące na Stwórcę. Dzięki
                                bezinteresownej pracy wielu wcześniejszych pokoleń, możemy odnaleźć ciszę, spokój i
                                harmonię w sakralnym wnętrzu naszej świątyni. Podobnie i dzisiaj, bez Twojej pomocy
                                Najpiękniejszy i najstarszy Kościół Opola nie będzie mógł zachwycać swoim pięknem.
                                Jeżeli zatem chciałbyś poczuć smak pracy zupełnie bezinteresownej a zarazem przyczynić
                                się do utrwalania ponadczasowego piękna zapraszamy do naszego wolontariatu.
                            </p>
                            <p>
                                Jeżeli zatem chciałbyś poczuć smak pracy zupełnie bezinteresownej a zarazem przyczynić
                                się do utrwalania ponadczasowego piękna zapraszamy do naszego wolontariatu.
                            </p>
                            <p style="text-align: center">
                                <a href="#">ZGŁASZAM SIĘ</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Ewangelizacja - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>