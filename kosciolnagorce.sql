-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 01 Cze 2020, 15:19
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `kosciolnagorce`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `adoracje`
--

CREATE TABLE `adoracje` (
  `id_adoracji` int(11) NOT NULL,
  `data` date NOT NULL,
  `imie` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `godzina` varchar(3) COLLATE utf8mb4_polish_ci NOT NULL,
  `minuty` varchar(3) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `adoracje`
--

INSERT INTO `adoracje` (`id_adoracji`, `data`, `imie`, `godzina`, `minuty`) VALUES
(1, '2020-06-02', 'Tomasz', '12', '00'),
(2, '2020-06-18', 'Tomasz', '15', '00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `aktualnosci`
--

CREATE TABLE `aktualnosci` (
  `id_aktualnosci` int(11) NOT NULL,
  `data_aktualnosci` datetime NOT NULL,
  `tytul_aktualnosci` text COLLATE utf8mb4_polish_ci DEFAULT NULL,
  `tresc_aktualnosci` text COLLATE utf8mb4_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `aktualnosci`
--

INSERT INTO `aktualnosci` (`id_aktualnosci`, `data_aktualnosci`, `tytul_aktualnosci`, `tresc_aktualnosci`) VALUES
(6, '2020-04-28 16:43:58', 'What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(8, '2020-04-28 16:50:58', 'What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(16, '2020-05-06 10:42:02', 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontakt`
--

CREATE TABLE `kontakt` (
  `id_kontaktu` int(11) NOT NULL,
  `data_kontaktu` datetime DEFAULT NULL,
  `imie_nazw_kontaktu` text COLLATE utf8mb4_polish_ci DEFAULT NULL,
  `email_kontaktu` text COLLATE utf8mb4_polish_ci DEFAULT NULL,
  `tresc_kontaktu` text COLLATE utf8mb4_polish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `kontakt`
--

INSERT INTO `kontakt` (`id_kontaktu`, `data_kontaktu`, `imie_nazw_kontaktu`, `email_kontaktu`, `tresc_kontaktu`) VALUES
(9, '2020-04-27 07:25:21', 'Tomasz Kosek', 'kosektomasz96@gmail.com', 'sdadasd'),
(10, '2020-04-27 07:25:33', 'sadasdas', 'asdasd@wp.pl', 'asdada'),
(11, '2020-04-27 07:25:55', 'sadasdas', 'asdasd@wp.pl', 'asdada'),
(12, '2020-04-27 07:26:13', 'sadasdas', 'asdasd@wp.pl', 'asdada'),
(13, '2020-04-27 07:26:19', 'Tomasz Kosek', 'kredkaluu88@gmail.com', 'sasdadasd'),
(14, '2020-05-06 12:05:37', 'Tomasz Kosek', 'kosektomas@wp.pl', 'Testowa wiadomosc');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `malzenstwo`
--

CREATE TABLE `malzenstwo` (
  `imie_slub` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `nazwisko_slub` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `data_slub` date NOT NULL,
  `godzina_slub` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email_slub` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `telefon_slub` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `deklaracja` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sakramenty`
--

CREATE TABLE `sakramenty` (
  `id` int(11) NOT NULL,
  `data_przeslania` date NOT NULL,
  `imie` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `nazwisko` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `nr_domu` varchar(10) COLLATE utf8mb4_polish_ci NOT NULL,
  `numer_mieszkania` varchar(10) COLLATE utf8mb4_polish_ci NOT NULL,
  `miejscowosc` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `kod_pocztowy` varchar(10) COLLATE utf8mb4_polish_ci NOT NULL,
  `szkola` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `klasa` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
  `imie_katechety` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `nazwisko_katechety` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `imie_zglaszajacego` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `nazwisko_zglaszajacego` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `email_zglaszajacego` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `numer_telefonu` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
  `nazwa_sakramentu` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `deklaracja` varchar(3) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `sakramenty`
--

INSERT INTO `sakramenty` (`id`, `data_przeslania`, `imie`, `nazwisko`, `ulica`, `nr_domu`, `numer_mieszkania`, `miejscowosc`, `kod_pocztowy`, `szkola`, `klasa`, `imie_katechety`, `nazwisko_katechety`, `imie_zglaszajacego`, `nazwisko_zglaszajacego`, `email_zglaszajacego`, `numer_telefonu`, `nazwa_sakramentu`, `deklaracja`) VALUES
(34, '2020-05-19', 'Tomasz', 'Nowak', 'Opolska', '1', '', 'Opole', '45-056', 'PSP 2', '3', 'Michał', 'Kostka', 'Piotr', 'Kowalski', 'piotr@email.com', '666999888', 'I KOMUNIA ŚWIĘTA', 'Tak'),
(35, '2020-05-19', 'Michał', 'Kowalski', 'Ozimska', '11', '3', 'Opole', '45-053', 'LO3', '4', 'Adam', 'Nowak', 'Wojciech', 'Kowalski', 'wojciech@email.com', '666777999', 'BIERZMOWANIE', 'Nie');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id_usera` int(11) NOT NULL,
  `nazwa_usera` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `email_usera` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `haslo_usera` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `admin_usera` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id_usera`, `nazwa_usera`, `email_usera`, `haslo_usera`, `admin_usera`) VALUES
(2, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `adoracje`
--
ALTER TABLE `adoracje`
  ADD PRIMARY KEY (`id_adoracji`);

--
-- Indeksy dla tabeli `aktualnosci`
--
ALTER TABLE `aktualnosci`
  ADD PRIMARY KEY (`id_aktualnosci`);

--
-- Indeksy dla tabeli `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`id_kontaktu`);

--
-- Indeksy dla tabeli `malzenstwo`
--
ALTER TABLE `malzenstwo`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `sakramenty`
--
ALTER TABLE `sakramenty`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_usera`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `adoracje`
--
ALTER TABLE `adoracje`
  MODIFY `id_adoracji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `aktualnosci`
--
ALTER TABLE `aktualnosci`
  MODIFY `id_aktualnosci` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT dla tabeli `kontakt`
--
ALTER TABLE `kontakt`
  MODIFY `id_kontaktu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT dla tabeli `malzenstwo`
--
ALTER TABLE `malzenstwo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `sakramenty`
--
ALTER TABLE `sakramenty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id_usera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
