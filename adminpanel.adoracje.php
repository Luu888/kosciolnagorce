<?php
if (@$_SESSION['admin'] == 1 && $zalogowany = 1) {
    $aktualnosci = 'select adoracje.* from adoracje order by data;';
    $z = $db->query($aktualnosci);
    ?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeIn">
                    <h1>INTERNETOWY ZESZYT ADORACJI</h1>
                    <div class="divider-h"><span></span></div>
                </div>
            </div>
        </div>
    </header>
    <article>
        <section class="zgloszenia-section">
            <div class="container">
                <div class="card-columns">
                    <?php
                    $licznik = 1;
                    foreach ($z as $zg) {
                        echo '
                    <div class="card">
                        <div class="card-body">						
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><b>Data adoracji: </b>' . $zg['data'] . '</li>
                                <li class="list-group-item"><b>Godzina: </b>' . $zg['godzina'] . ':' . $zg['minuty'] . '</li>
                                <li class="list-group-item"><b>Imię wstawiającego: </b>' . $zg['imie'] . '</li>
                                <li class="list-group-item"><button class="btn btn-outline-danger btn-block" data-toggle="modal" data-target="#exampleModal">Usuń</button></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Uwaga!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Czy na pewno chcesz usunąć to zgłoszenie?
                                </div>
                                <div class="modal-footer">
                                    <a type="button" class="btn btn-danger" href="./adminpanel.adoracje/' . $zg['id_adoracji'] . '/usun/" >Tak</a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Nie</button>
                                </div>
                            </div>
                        </div>
                    </div>';
                    }
                    if (isset($params[1]) && $params[1] == 'usun') {
                        $query_usun = 'DELETE FROM adoracje where id_adoracji=' . $params[0];
                        $db->query($query_usun);
                        header('Location: /kosciolnagorce/adminpanel.adoracje/');
                    }

                    ?>
                </div>
            </div>
        </section>
    </article>
    <?php
} else {
    include 'blad.php';
}
?>
