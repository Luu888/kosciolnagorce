<?php
$wyslano = false;
if (isset($_POST['adoracjaImie']) && isset($_POST['adoracjaData']))
	{
		$data = $_POST['adoracjaData'];
		$imie = $_POST['adoracjaImie'];
		$godzina = $_POST['adoracjaGodzina'];
		$minuty = $_POST['adoracjaMinuty'];
		$czyjest = $db->query('Select count(*) as ilosc from adoracje where data="'.$data.'" AND godzina="'.$godzina.'" AND minuty="'.$minuty.'";');
		$licz = $czyjest->fetch_array(MYSQLI_ASSOC);
		$wyniczek = $licz['ilosc'];
		if($wyniczek==0)
		{
			$query = 'insert into adoracje values(null, "'.$data.'", "'.$imie.'", "'.$godzina.'", "'.$minuty.'")';
				$db->query($query);

			echo '<div class="alert alert-secondary wow pulse" role="alert">
				Dziękujemy za przesłanie formularza adoracyjnego! :)
			</div>';
		}
		else
			{
				echo '<div class="alert alert-secondary wow pulse" role="alert">
				Niestety ten termin jest już zajęty, wybierz inny termin!
				</div>';
			}
	}
?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>INTERNETOWY ZESZYT ADORACJI</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>


    <article>
        <section class="adoracja-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <img src="assets/img/adoracja.png" style="margin-bottom: 30px;">
                    </div>
                    <div class="col-12" style="margin-bottom: 30px;">
                        <div class="adoracja-content">
                            <p>
                                Internetowy Zeszyt Adoracji pozwoli Ci określić czas, w którym masz możliwość adoracji
                                Najświętszego Sakramentu w naszej świątyni. W formularzu podaj swoje imię, wybierz datę
                                oraz godzinę. Adoracja jest możliwa od poniedziałku do piątku w godzinach 7.00 - 17.30
                                (w środy do godziny 17.00, w związku z Nowenną do MB Nieustającej Pomocy). Formularz
                                umożliwia zapisy na 15-minutowy czas adoracji.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6" style="margin-bottom: 10px;">
                        <div class="adoracja-content">
                            <form method="POST" action="./adoracja">
                                <h1>ZAPISZ SIĘ W ZESZYCIE ADORACJI</h1>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="adoracjaImie"
                                           placeholder="Imię" name="adoracjaImie" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" onfocus="(this.type='date')"
                                           id="adoracjaData" name="adoracjaData" placeholder="Data">
                                </div>
                                <p style="font-size: 1.0rem; padding: 5px;">Pamiętaj, że w środy ostatnią godziną, na
                                    którą powinieneś
                                    się zapisać jest
                                    16:45.</p>
                                <div class="form-group">
                                    <select id="adoracjaGodzina" name="adoracjaGodzina" class="form-control"
                                            style="color: #737373;">
                                        <option value="" disabled selected >Godzina</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        
                                    </select>
									<select id="adoracjaMinuty" name="adoracjaMinuty" class="form-control"
                                            style="color: #737373;">
                                        <option value="" disabled selected >Minuty</option>
                                        <option value="00">00</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                        <option value="45">45</option>                                       
                                    </select>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"
                                           id="zgodaDaneM" name="zgodaDane" required>
                                    <label class="custom-control-label" for="zgodaDaneM">Wyrażam zgodę
                                        na przetwarzanie danych osobowych</label>
                                </div>
                                <button type="submit" class="btn btn-block btn-secondary"
                                        style="margin-top: 15px;">Wyślij
                                    formularz
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 " >
                        <div class="adoracja-content" style="min-height: 507px;">
                            <h1>ZAJĘTE TERMINY</h1>
                            <div style="height: 357px;  overflow-y:scroll;">
								<?php
									$adoracje = 'select adoracje.* from adoracje order by data';
									$z = $db->query($adoracje);
									foreach ($z as $zg)
										{
                                            echo '  <div class="card">
                                                      <div class="card-body">
                                                        <b>Data: </b>'.$zg['data'].'
                                                        <b>Godzina: </b>'.$zg['godzina'].':'.$zg['minuty'].'
                                                      </div>
                                                    </div>';

										}
								
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Zeszyt Adoracji - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>