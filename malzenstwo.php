<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>MAŁŻEŃSTWO</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                Miłość cierpliwa jest, łaskawa jest… miłość nigdy nie ustaje
                            </p>
                            1 Kor 13,4
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/malzenstwo.jpg" style="margin-bottom: 30px;">
                        <div class="sakramenty-content">
                            <p>
                                Jeżeli chcesz, aby Twoja relacja z najbliższą osobą została wzmocniona miłością Boga i Jego
                                błogosławieństwem zachęcamy do przyjęcia sakramentu małżeństwa.
                            </p>
                            <p>
                                Śluby w naszej parafii odbywają się w dni powszednie od poniedziałku do czwartku oraz w
                                soboty po wcześniejszym ustaleniu terminu. Aby otrzymać niezbędne informacje w sprawie
                                przygotowań do ślubu warto przyjść albo zadzwonić do kancelarii w godzinach jej otwarcia.
                            </p>
                            <p>
                                Numer telefonu oraz pozostałe informacje kontaktowe znajdziesz <a href="kontakt">TUTAJ.</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Małżeńśtwo - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>