<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>WOLONTARIAT - #CHCĘ POMÓC</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="wolontariat-section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <img src="assets/img/wolontariat.jpg" style="margin-bottom: 30px;">
                        <div class="wolontariat-content">
                            <p>
                                W dzisiejszym świecie często skoncentrowanym na osiąganych zyskach chcemy Ci
                                zaproponować posługę niedającą żadnych korzyści. Jedyne co osiągniesz to świadectwo
                                sumienia, że wykonałeś coś zupełnie bezinteresownie. można taką posługę pełnić w
                                niezliczonych codziennych sytuacjach.
                            </p>
                            <p>
                                Dlaczego proponujemy nasz wolontariat? Nasza świątynia będąca najstarszą sakralną
                                budowlą Opola stoi dzięki ludziom, którzy nie
                                licząc na własne korzyści pragnęli troszczyć się o piękno wskazujące na Stwórcę. Dzięki
                                bezinteresownej pracy wielu wcześniejszych pokoleń, możemy odnaleźć ciszę, spokój i
                                harmonię w sakralnym wnętrzu naszej świątyni. Podobnie i dzisiaj, bez Twojej pomocy
                                Najpiękniejszy i najstarszy Kościół Opola nie będzie mógł zachwycać swoim pięknem.
                                Jeżeli zatem chciałbyś poczuć smak pracy zupełnie bezinteresownej a zarazem przyczynić
                                się do utrwalania ponadczasowego piękna zapraszamy do naszego wolontariatu.
                            </p>
                            <p>
                                Jeżeli zatem chciałbyś poczuć smak pracy zupełnie bezinteresownej a zarazem przyczynić
                                się do utrwalania ponadczasowego piękna zapraszamy do naszego wolontariatu.
                            </p>
                            <p style="text-align: center">
                                <a href="#">ZGŁASZAM SIĘ</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Wolontariat - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>