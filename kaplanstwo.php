<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>KAPŁAŃSTWO</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>

    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                Przechodząc obok Jeziora Galilejskiego, ujrzał Szymona i brata Szymonowego, Andrzeja jak
                                zarzucali sieć w jezioro, byli bowiem rybakami. I rzekł do nich Jezus: «Pójdźcie za Mną,
                                a sprawię, że się staniecie rybakami ludzi». A natychmiast porzuciwszy sieci poszli za
                                Nim
                            </p>
                            Mk 1,16-18
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/kaplanstwo.jpg" style="margin-bottom: 30px;">
                        <div class="sakramenty-content">
                            <p>
                                Być może Bóg przygotował także i dla Ciebie misję której zupełnie się nie spodziewałeś?
                                Jeżeli rzeczywiście stawiasz sobie pytania dotyczące powołania kapłańskiego lub
                                zakonnego,
                                przyjdź do naszej kancelarii, spróbujemy Ci pomóc.
                            </p>
                            <p>
                                W naszej parafii na głos Bożego powołania odpowiedzieli:
                            </p>
                            <p>
                                <b>KAPŁANI</b><br>
                                ks. Wiktor Sobaszek, ks. Bronisław Czajka, ks. Edward Kucharz, ks. Jan Szyndler,
                                ks. Ryszard Szwarc, ks. Paweł Felsztyński, ks. Ireneusz Kotliński, ks. Dariusz Świerc,
                                o.
                                Ambroży Mazur, o. Andrzej Mazur, o. Krzysztof Mazur, ks. Tomasz Malicki, ks. Tomasz
                                Skowron.
                            </p>
                            <p>
                                <b>SIOSTRY ZAKONNE</b><br>
                                s. Ambrozja Mazur SSND.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Kapłaństwo - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>