<?php
$aktualnosci = 'SELECT * FROM `aktualnosci` order by `id_aktualnosci` desc limit 3;';
$z = $db->query($aktualnosci);
?>
<article>
    <section class="glowna-section" style="padding: 0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-8 slider">
                    <div id="sliderPrzelaczniki" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#sliderPrzelaczniki" data-slide-to="0" class="active"></li>
                            <li data-target="#sliderPrzelaczniki" data-slide-to="1"></li>
                            <li data-target="#sliderPrzelaczniki" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <a href="adoracja">
                                    <img class="d-block w-100" src="assets/img/glowna/slider/slider1.png"
                                         alt="Adoracja">
                                </a>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/glowna/slider/slider2.png"
                                     alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="assets/img/glowna/slider/slider3.png" alt="Third slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-4 news">
                    <?php
                    foreach ($z as $zg) {
                        echo '<div class="card aktualnosc">';
                        if (strlen($zg['tytul_aktualnosci']) >= 40) {
                            echo '<h1>' . substr($zg['tytul_aktualnosci'], 0, 40) . '...</h1>';
                        } else {
                            echo '<h1>' . $zg['tytul_aktualnosci'] . '</h1>';
                        }
                        echo '<p class="data">
                                DODANO:' . $zg['data_aktualnosci'] .
                            '</p>';
                        $tresc = $zg['tresc_aktualnosci'];
                        if (strlen($tresc) >= 77) {
                            echo '<p class="tekst">' . substr($tresc, 0, 77) . '...</p>';
                        } else {
                            echo $tresc;
                        }
                        echo '<a href="./wpis/' . $zg['id_aktualnosci'] . '/">CZYTAJ WIĘCEJ</a></div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="glowna-section">
        <div class="container">
            <div class="row">
                <div class="col-6 col-lg-3 gray-bg">
                    <div class="glowna-content info">
                        <h1>MSZA ŚWIĘTA</h1>
                        <p><b>PONIEDZIAŁEK - PIĄTEK</b></p>
                        <p>6:30 | 12:00 | 18:00</p>
                        <p><b>SOBOTA</b></p>
                        <p>6:30 | 18:00</p>
                        <p><b>NIEDZIELA</b></p>
                        <p>7:30 | 9:00 | 10:30 | 12:00 | 18:00</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3 gray-bg">
                    <div class="glowna-content info">
                        <h1>SPOWIEDŹ</h1>
                        <p><b>PONIEDZIAŁEK - PIĄTEK</b></p>
                        <p>6:10 - 6:25 | 17:30 - 17:45</p>
                        <p><b>SOBOTA</b></p>
                        <p>6:10 - 6:25 | 17:00 - 17:45</p>
                        <p><b>NIEDZIELA</b></p>
                        <p>pół godziny przed każdą mszą świętą</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3 gray-bg">
                    <div class="glowna-content info">
                        <h1>KANCELARIA</h1>
                        <p><b>PONIEDZIAŁEK - ŚRODA - PIĄTEK</b></p>
                        <p>16:00 - 16:45</p>
                        <p><b>WTOREK - CZWARTEK</b></p>
                        <p>10:00 - 11:00</p>
                    </div>
                </div>
                <div class="col-6 col-lg-3 gray-bg">
                    <div class="glowna-content info">
                        <h1>KONTO PARAFII</h1>
                        <p>18 1140 2004 0000 3102 7945 8833</p>
                        <p><b>TYTUŁ WPŁATY</b></p>
                        <p style="line-height: 25px; margin-bottom: 10px;"><b>"Darowizna na cele kultu religijnego"</b>
                        </p>
                        <p style="line-height: 25px; margin-bottom: 10px;">Serdeczne Bóg zapłać za wszystkie Wasze
                            ofiary
                            na rzecz naszej
                            Parafii</p>
                    </div>
                </div>
            </div>
    </section>
    <section>
        <div class="parallax" style="background-image: url('assets/img/glowna/paralaksa/para1.jpg');"></div>
    </section>
    <section class="glowna-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-4 gray-bg">
                    <div class="glowna-content">
                        <i class="flaticon-045-scroll"></i>
                        <h1>INTENCJA ZBIOROWA</h1>
                        <p>Pod tym <a href="formularze">linkiem</a> znajdziesz formularz, który umożliwia zamówienie
                            zbiorowej intencji mszalnej.
                            Zapraszamy do skorzystania.</p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 gray-bg">
                    <div class="glowna-content">
                        <i class="flaticon-011-baptism"></i>
                        <h1>CHRZEST</h1>
                        <p>Sakrament chrztu odbywa się w każdą niedzielę po mszy św. sprawowanej o godz. 12.00.
                            Więcej informacji na temat chrztu oraz formularz zgłoszeniowy znajdziesz <a
                                    href="chrzest">tutaj</a>.
                    </div>
                </div>
                <div class="col-12 col-lg-4 gray-bg">
                    <div class="glowna-content">
                        <i class="flaticon-016-cross"></i>
                        <h1>POGRZEB</h1>
                        <p>Jeżeli potrzebujesz naszej pomocy w bolesnym doświadczeniu śmierci bliskiej osoby <a
                                    href="kontakt">skontaktuj się</a> z nami lub przyjdź do naszej kancelarii
                            parafialnej.
                        </p>
                    </div>
                </div>
            </div>
    </section>
    <section>
        <div class="parallax" style="background-image: url('assets/img/glowna/paralaksa/para2.jpg');"></div>
    </section>
    <section class="glowna-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-4 gray-bg">
                    <div class="glowna-content">
                        <i class="flaticon-009-communion"></i>
                        <h1>I KOMUNIA ŚWIĘTA</h1>
                        <p>Najpiękniejszy dar jaki może otrzymać Twoje dziecko to żywa relacja z Jezusem
                            Eucharystycznym. Pod tym <a href="formularze">linkiem</a> znajdziesz informacje o
                            przygotowaniach oraz formularz dla
                            rodziców zgłaszających swoje dziecko do przygotowań.</p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 gray-bg">
                    <div class="glowna-content">
                        <i class="flaticon-010-candle"></i>
                        <h1>ROCZEK</h1>
                        <p>W naszej parafii błogosławieństwo rocznych dzieci odbywa się
                            zazwyczaj po mszy świętej sprawowanej w niedzielę o godz. 12.00 ale jest możliwe także po
                            zakończeniu każdej mszy świętej. Aby zgłosić roczne dziecko do błogosławieństwa skorzystaj z
                            dostępnego <a href="formularze">formularza</a> lub <a
                                    href="kontakt">skontaktuj się</a> z nami telefonicznie w
                            godzinach otwarcia
                            kancelarii. </p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 gray-bg">
                    <div class="glowna-content">
                        <i class="flaticon-048-holy-ghost"></i>
                        <h1>BIERZMOWANIE</h1>
                        <p>Sakrament bierzmowania to moment udzielenia człowiekowi siedmiu darów Ducha Świętego. Pod tym
                            linkiem znajdziesz <a href="formularze">formularz</a> zgłoszeniowy oraz informacje o
                            przygotowaniach do
                            bierzmowania</p>
                    </div>
                </div>
            </div>
    </section>

    <section>
        <div class="parallax" style="background-image: url('assets/img/glowna/paralaksa/para3.jpg');"></div>
    </section>
    <section class="glowna-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 gray-bg">
                    <div class="glowna-content">
                        <h1>NOWENNA DO MBNP</h1>
                        <p>To nabożeństwo o najdłuższej tradycji w naszej parafii. Odprawiane w każdą środę o godz.
                            17.15 w kaplicy Matki Boskiej Częstochowskiej. Podczas nowenny do MBNP czytane są
                            intencje wiernych składane w naszym kościele albo za pośrednictwem formularza
                            online. </p>
                    </div>
                </div>
                <div class="col-12 col-lg-6 gray-bg">
                    <div class="glowna-content">
                        <h1>KRUCJATA RÓŻAŃCOWA ZA OJCZYZNĘ</h1>
                        <p>W każdą trzecią środę miesiąca podczas mszy św. sprawowanej o godz. 18.00 gościmy w
                            naszej parafii uczestników Krucjaty Różańcowej. Po zakończeniu mszy chętni wyruszają na
                            ulice naszego miasta aby modlitwą różańcową prosić Boga o pomyślność naszej Ojczyzny i
                            silną wiarę w narodzie. </p>
                    </div>
                </div>
                <div class="col-12 gray-bg" style="margin-top: 10px;">
                    <div class="glowna-content">
                        <h1>NABOŻEŃSTWA OKRESOWE</h1>
                        <P>Do NSPJ sprawowane w pierwsze piątki miesiąca o godz. 17.30</P>
                        <P>Godzina Święta w pierwsze czwartki miesiąca o godz. 17.30</P>
                        <P>Wynagradzające Niepokalanemu Sercu Najświętszej Maryi Panny w pierwsze soboty miesiąca o
                            godz. 17.30
                        </P>
                        <p>Majowe codzienne o godz. 17.30</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="glowna-section" style="padding-top: 0;">
        <div class="container">
            <h1>WSPIERAJĄ NAS</h1>
            <div class="swiper-container" style="margin: 20px 0; background-color: #F7F7F7; padding: 30px 0;">
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/cepelianka.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/maxbau.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/technopoz.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/cepelianka.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/maxbau.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/technopoz.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/cepelianka.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/maxbau.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/technopoz.png"></div>
                    <div class="swiper-slide"><img style="height: 100px;" src="assets/img/glowna/swaper/cepelianka.png"></div>
                </div>
            </div>
        </div>
    </section>
</article>

