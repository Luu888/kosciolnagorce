<?php

?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeInDown">
                    <h1>CHRZEST</h1>
                    <div class="divider-h wow fadeInDown"><span></span></div>
                </div>
            </div>
        </div>
    </header>


    <article>
        <section class="sakramenty-section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sakramenty-cytat">
                            <p>
                                Przez chrzest zanurzający nas w śmierć Chrystusa zostaliśmy z Nim pogrzebani abyśmy i my
                                wkroczyli w nowe życie
                            </p>
                            Rz 6,4
                        </div>
                    </div>
                    <div class="col-12">
                        <img src="assets/img/sakramenty/chrzest.jpg" style="margin-bottom: 30px;">
                        <div class="sakramenty-content">
                            <p>
                                Sakrament chrztu w naszej parafii jest udzielany w każdą niedzielę bezpośrednio po Mszy
                                św.
                                sprawowanej o godz. 12.00 Aby zgłosić dziecko do chrztu zadzwoń do nas w godzinach
                                otwarcia
                                kancelarii, powiemy Ci co zrobić dalej :)
                            </p>
                            <p>
                                Numer telefonu oraz pozostałe informacje kontaktowe znajdziesz <a
                                        href="kontakt">tutaj.</a>
                            </p>
                            <p>
                                Swoje dziecko możesz również zgłosić do sakramentu chrztu korzystając z formularza,
                                który znajdziesz pod tym <a href="formularze">linkiem</a>.
                            </p>
                            <p>
                                <b>Nauka przedchrzcielna odbywa się w I piątek miesiąca o godz. 19.00 w kancelarii
                                    parafialnej.</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </article>
<?php
$pageTitle = 'Chrzest - Parafia "na Górce"';
?>
    </html>
<?php
include 'title.php';
?>