<?php
$tytulek = $params[0];
$tytul = urldecode($tytulek);
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (@$_SESSION['admin'] == 1 && $zalogowany = 1) {
    ?>
    <header>
        <div class="container">
            <div class="row">
                <div class="col wow fadeIn">

                    <h1><?php echo $tytul; ?></h1>
                    <div class="divider-h"><span></span></div>
                </div>

            </div>

        </div>
    </header>
    <?php
//echo "<a href='./adminpanel.galeria.edytuj/dodaj/".$tytul."/'><button class='btn btn-outline-success btn' type='submit'>Dodaj zdjęcia</button></a>";
    if (isset($params[0]) && $params[0] == 'dodaj') {
        $target_dir = "gallery/" . urldecode($params[1]) . "/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($_FILES["fileToUpload"]["size"] > 50000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" && $imageFileType != "JPG" && $imageFileType != "JPEG" && $imageFileType != "PNG") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
    if (isset($params[0]) && $params[0] == 'usun') {
        unlink('gallery/' . urldecode($params[1] . '/' . urldecode($params[2])));
        header("Location: /kosciolnagorce/adminpanel.galeria.edytuj/" . $params[1]);

    }
    ?>

    <article>
        <div class="galeria-section">
            <div class="container">
                <h2>Dodaj kolejne zdjęcie:</h2>
                <div class="card" style="margin: 20px auto; width: 70%;">
                    <div class="card-body">
                        <form action="./adminpanel.galeria.edytuj/dodaj/<?php echo $tytul; ?>/" method="post"
                              enctype="multipart/form-data">
                            <div class="row">
                                <div class="col">
                                    <input type="file" name="fileToUpload" id="fileToUpload">
                                </div>
                                <div class="col">
                                    <input type="submit" value="Dodaj zdjęcie" name="submit">
                                </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <?php
                    $imagesDirectory = "gallery/" . $tytul;

                    if (is_dir($imagesDirectory)) {
                        $opendirectory = opendir($imagesDirectory);

                        while (($image = readdir($opendirectory)) !== false) {
                            if (($image == '.') || ($image == '..')) {
                                continue;
                            }

                            $imgFileType = pathinfo($image, PATHINFO_EXTENSION);

                            if (($imgFileType == 'jpg') || ($imgFileType == 'png') || ($imgFileType == 'JPG') || ($imgFileType == 'JPEG') || ($imgFileType == 'PNG') || ($imgFileType == 'jpeg')) {
                                echo '<div class="col-6 col-md-3">
                                    <a href="./gallery/' . $tytul . '/' . $image . '" data-toggle="lightbox" data-gallery="gallery"><img src="gallery/' . $tytul . '/' . $image . '" width="200"></a>
                                    <a href="./adminpanel.galeria.edytuj/usun/' . $tytul . '/' . $image . '/">Usuń</a><br>
                                  </div>
                                  ';
                            }
                        }
                        closedir($opendirectory);
                    }
                    if (isset($params[0]) && $params[0] == 'dodaj') {
                        $path = "gallery/" . $_GET['tytul'];

                        mkdir("./gallery/" . $_GET['tytul'], 0755, true);
                        //echo $path;
                        header('Location: /kosciolnagorce/adminpanel.galeria.edytuj/' . $params[1] . "/");
                    }
                    ?>
                </div>
            </div>
        </div>
    </article>
    <?php
} else {
    include 'blad.php';
}
?>