<?php

$wyslano = false;

if (isset($_POST['imie_slub']) && isset($_POST['nazwisko_slub'])) {
    $imie = $_POST['imie_slub'];
    $nazwisko = $_POST['nazwisko_slub'];
    $email = $_POST['email_slub'];
    $telefon = $_POST['telefon_slub'];
    $godzina = $_POST['godzina_slub'];
    $data = $_POST['data_slub'];
   
    $query = 'insert into malzenstwo values("' .  $imie . '", "' . $nazwisko . '", "' . $data . '", "' . $godzina . '", "'.$email.'", "'.$telefon.'", "TAK", null)';
    
    $db->query($query);

    echo '<div class="alert alert-secondary wow pulse" role="alert">
            Dziękujemy za przesłanie wiadomości. Odpowiemy najszybciej jak to możliwe! :)
          </div>';
}
?>

<article>
<div class="formularze-content">
                                <form method="POST" action="./dodaj_malzenstwo">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <h1>WYBÓR DATY ŚLUBU</h1>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" onfocus="(this.type='date')"
                                                           id="data_slub" name="data_slub" placeholder="Data">
                                                </div>
                                                <div class="form-group">
                                                    <select id="slubGodzina" name="godzina_slub" class="form-control"
                                                            style="color: #737373;">
                                                        <option value="" disabled selected>Godzina</option>
                                                        <option value="11:00">11:00</option>
                                                        <option value="12:00">12:00</option>
                                                        <option value="13:00">13:00</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <h1>DANE OSOBY ZGŁASZAJĄCEJ</h1>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="imie_slub"
                                                           placeholder="Imię zgłaszającego" name="imie_slub"
                                                           required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="nazwisko_slub"
                                                           placeholder="Nazwisko zgłaszającego"
                                                           name="nazwisko_slub" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="email_slub"
                                                           placeholder="Adres email" name="email_slub" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="tel" class="form-control" id="telefon_slub"
                                                           placeholder="Numer telefonu" name="telefon_slub"
                                                           required>
                                                </div>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input"
                                                           id="zgodaDaneM" name="zgodaDane" required>
                                                    <label class="custom-control-label" for="zgodaDaneM">Wyrażam zgodę
                                                        na przetwarzanie danych osobowych</label>
                                                </div>
                                                
                                                
                                                <button type="submit" class="btn btn-block btn-secondary"
                                                        style="margin-top: 10px;">Wyślij
                                                    formularz
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
</article>