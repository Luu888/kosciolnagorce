<?php
$tytulek = $params[0];
$tytul = str_replace("%20", " ", $tytulek);

?>
<header>
    <div class="container">
        <div class="row">
            <div class="col wow fadeIn">
                <h1><?php echo $tytul; ?></h1>
                <div class="divider-h"><span></span></div>
            </div>
        </div>
    </div>
</header>


<article>
    <div class="galeria-section">
        <div class="container">
            <?php
            $imagesDirectory = "gallery/" . $tytul;

            if (is_dir($imagesDirectory)) {
                $opendirectory = opendir($imagesDirectory);

                while (($image = readdir($opendirectory)) !== false) {
                    if (($image == '.') || ($image == '..')) {
                        continue;
                    }

                    $imgFileType = pathinfo($image, PATHINFO_EXTENSION);

                    if (($imgFileType == 'jpg') || ($imgFileType == 'png')) {
                        echo "<a href='./gallery/" . $tytul . "/" . $image . "' data-toggle='lightbox' data-gallery='gallery'><img src='gallery/" . $tytul . "/" . $image . "' width='200'></a>";
                    }
                }

                closedir($opendirectory);

            }
            ?>
        </div>
    </div>
</article>
